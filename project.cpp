////////////////////////////////////////////////////////////////////////
//
//   Michelle Luo and Megan Quintero
//   Harvard University
//   CS175 : Computer Graphics
//   Professor Steven Gortler
//   Final Project 2011
//
////////////////////////////////////////////////////////////////////////

#include <string>
#include <vector>
#include <list>
#include <memory>
#include <stdexcept>
#include <iostream>
#include <fstream>
#if __GNUG__
#   include <tr1/memory>
#endif

#include <GL/glew.h>
#ifdef __MAC__
#   include <GLUT/glut.h>
#else
#   include <GL/glut.h>
#endif

#include "asstcommon.h"
#include "cvec.h"
#include "drawer.h"
#include "geometry.h"
#include "glsupport.h"
#include "matrix4.h"
#include "particle.h"
#include "ppm.h"
#include "quat.h"
#include "rigtform.h"
#include "scenegraph.h"
#include "sgutils.h"
#include "geometrymaker.h"
#include "material.h"
#include "renderstates.h"
#include "texture.h"
#include "uniforms.h"
#include "mesh.h"

using namespace std;      // for string, vector, iostream, and other standard C++ stuff
using namespace std::tr1; // for shared_ptr

// G L O B A L S ///////////////////////////////////////////////////

// --------- IMPORTANT --------------------------------------------------------
// Before you start working on this assignment, set the following variable
// properly to indicate whether you want to use OpenGL 2.x with GLSL 1.0 or
// OpenGL 3.x+ with GLSL 1.3.
//
// Set g_Gl2Compatible = true to use GLSL 1.0 and g_Gl2Compatible = false to
// use GLSL 1.3. Make sure that your machine supports the version of GLSL you
// are using. In particular, on Mac OS X currently there is no way of using
// OpenGL 3.x with GLSL 1.3 when GLUT is used.
//
// If g_Gl2Compatible=true, shaders with -gl2 suffix will be loaded.
// If g_Gl2Compatible=false, shaders with -gl3 suffix will be loaded.
// To complete the assignment you only need to edit the shader files that get
// loaded
// ----------------------------------------------------------------------------
const bool g_Gl2Compatible = true;

static const float g_frustFovY = 60.0;    // 60 degree field of view in y direction
static const float g_frustNear = -0.1;    // near plane
static const float g_frustFar = -50.0;    // far plane
static const float g_groundY = -2.0;      // y coordinate of the ground
static const float g_groundSize = 10.0;   // half the ground length
static const float g_cameraDistance = 6.0;

static int g_numFiles = 0;

static int g_windowWidth = 512;
static int g_windowHeight = 512;
static bool g_mouseClickDown = false;    // is the mouse button pressed
static bool g_mouseLClickButton, g_mouseRClickButton, g_mouseMClickButton;

static int g_mouseClickX, g_mouseClickY; // coordinates for mouse click event
static int g_activeShader = 0;

inline void print(const Matrix4& m) {
  for (int i = 0; i < 4; i++)
  {
    for (int j = 0; j < 4; j++)
      cout << m(i,j) << ", ";
    cout << "\n";
  }
}

static shared_ptr<Material> g_greenDiffuseMat,
			    g_lightMat,
			    g_fireworkMat;

shared_ptr<Material> g_overridingMaterial;

// Vertex buffer and index buffer associated with the ground and cube geometry
static shared_ptr<Geometry> g_ground, g_sphere, g_light;

// testing for dynamic geometry
static shared_ptr<ParticleGeometryPN> g_mainFirework;
static shared_ptr<NonIndexedDynamicGeometryPN> g_firework1, g_firework2;

static Mesh g_m, g_main;
static Mesh g_firework1Mesh, g_firework2Mesh;

// --------- Scene

static const Cvec3 g_light1(2.0, 3.0, 14.0); 
static RigTForm g_skyRbt = RigTForm(Cvec3(0.0, 0.25, 4.0));

// objects
static shared_ptr<SgRootNode> g_world;
static shared_ptr<SgRbtNode> g_skyNode, g_groundNode;
static shared_ptr<SgRbtNode> g_lightNode;

static shared_ptr<SgRbtNode> g_fireworkNode;

// particle systems
static list<Particle> g_fireworkParticles;

// eye
static shared_ptr<SgRbtNode> g_eye;

static void setEye() 
{
  g_eye = g_skyNode;
  return;
}

// timing/animation
static int g_msBetweenKeyFrames = 2000;       // 2 seconds between keyframes
static int g_animateFramesPerSecond = 60;     // frames to render per second during animation playback
static int g_animateMeshFramesPerSecond = 10; // frames to render per second for mesh animation

// boolean for fireworkk mode
static int g_currentFirework = 2;

///////////////// END OF G L O B A L S //////////////////////////////////////////////////

static void initGround() {
  int ibLen, vbLen;
  getPlaneVbIbLen(vbLen, ibLen);

  // Temporary storage for cube GeometryPNTBX
  vector<VertexPNTBX> vtx(vbLen);
  vector<unsigned short> idx(ibLen);

  makePlane(g_groundSize*2, vtx.begin(), idx.begin());
  g_ground.reset(new GeometryPNTBX(&vtx[0], &idx[0], vbLen, ibLen));
}

// the centers of the fireworks are spheres
static void initSpheres() {
  int ibLen, vbLen;
  getSphereVbIbLen(20, 10, vbLen, ibLen);

  // Temporary storage for sphere GeometryPNTBX
  vector<VertexPNTBX> vtx(vbLen);
  vector<unsigned short> idx(ibLen);
  makeSphere(0.5, 20, 10, vtx.begin(), idx.begin());
  g_sphere.reset(new GeometryPNTBX(&vtx[0], &idx[0], vtx.size(), idx.size()));
}

static void dumpMesh(vector<VertexPN>* vs, Mesh* m, int numFaces) {
  for (int i = 0; i < numFaces; i++)
  {
    Mesh::Face face = (*m).getFace(i);

    bool quad = (face.getNumVertices() == 4);

    Cvec3 p1 = (face.getVertex(0).getPosition())*0.1;
    Cvec3 p2 = (face.getVertex(1).getPosition())*0.1;
    Cvec3 p3 = (face.getVertex(2).getPosition())*0.1;
    Cvec3 p4;
    if (quad)
      p4 = (face.getVertex(3).getPosition())*0.1;

    Cvec3 n1 = face.getNormal();
    Cvec3 n2 = n1;
    Cvec3 n3 = n1;
    Cvec3 n4 = n1;

    VertexPN vpn1 = VertexPN(p1[0],p1[1],p1[2],n1[0],n1[1],n1[2]);
    VertexPN vpn2 = VertexPN(p2[0],p2[1],p2[2],n2[0],n2[1],n2[2]);
    VertexPN vpn3 = VertexPN(p3[0],p3[1],p3[2],n3[0],n3[1],n3[2]);
    VertexPN vpn4 = VertexPN(p4[0],p4[1],p4[2],n4[0],n4[1],n4[2]);

    face.getVertex(0).setNormal(n1);
    face.getVertex(1).setNormal(n2);
    face.getVertex(2).setNormal(n3);
    if (quad)
      face.getVertex(3).setNormal(n4);

    (*vs).push_back(vpn1);
    (*vs).push_back(vpn2);
    (*vs).push_back(vpn3);

    if (quad)
    {
      (*vs).push_back(vpn1);
      (*vs).push_back(vpn3);
      (*vs).push_back(vpn4);
    }
  }
}
static void initMesh() {

  g_m = Mesh();
  g_m.load("cube.mesh");

  g_firework1Mesh = g_m;

  for (int i=0; i<2; i++)
    g_firework1Mesh.subdivide();

  int numFaces = g_firework1Mesh.getNumFaces();
  vector<VertexPN> vs1;
  dumpMesh(&vs1, &g_firework1Mesh, numFaces);

  g_firework1.reset(new NonIndexedDynamicGeometryPN(vs1.size()));
  g_firework1->reset(&vs1[0], vs1.size());

  g_firework2Mesh = g_m;

  for (int i=0; i<4; i++)
    g_firework2Mesh.subdivide();

  numFaces = g_firework2Mesh.getNumFaces();
  vector<VertexPN> vs2;
  dumpMesh(&vs2, &g_firework2Mesh, numFaces);

  g_firework2.reset(new NonIndexedDynamicGeometryPN(vs2.size()));
  g_firework2->reset(&vs2[0], vs2.size());
}

static void initLights() {
  int ibLen, vbLen;
  getSphereVbIbLen(20, 10, vbLen, ibLen);

  // Temporary storage for sphere GeometryPNTBX
  vector<VertexPNTBX> vtx(vbLen);
  vector<unsigned short> idx(ibLen);
  makeSphere(0.2, 20, 10, vtx.begin(), idx.begin());
  g_light.reset(new GeometryPNTBX(&vtx[0], &idx[0], vtx.size(), idx.size()));
}

static void drawStuff() {

  // Declare an empty uniforms
  Uniforms uniforms;

  // build & send proj. matrix to be stored by uniforms, 
  // as opposed to the current vtx shader
  
  const Matrix4 projmat = Matrix4::makeProjection(
    g_frustFovY, g_windowWidth / static_cast <double> (g_windowHeight),
    g_frustNear, g_frustFar);
  
  sendProjectionMatrix(uniforms, projmat);

  // use the current eye as the eyeRbt
  g_eye = g_skyNode;
  const RigTForm eyeRbt = getPathAccumRbt(g_world,g_eye);
  const RigTForm invEyeRbt = inv(eyeRbt);

  Cvec3 light = getPathAccumRbt(g_world, g_lightNode).getTranslation();
    const Cvec3 eyeLight = Cvec3(invEyeRbt * Cvec4(light,1)); 
    uniforms.put("uLight", eyeLight);
    Drawer drawer(invEyeRbt, uniforms);
    g_world->accept(drawer);
  
}

static void display() {
  // No more glUseProgram
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  drawStuff(); // no more curSS

  glutSwapBuffers();

  checkGlErrors();
}

static void reshape(const int w, const int h) {
  g_windowWidth = w;
  g_windowHeight = h;
  glViewport(0, 0, w, h);
  std::cerr << "Size of window is now " << w << "x" << h << std::endl;
  glutPostRedisplay();
}

static void initFirework(Mesh* fireworkMesh, Cvec3 fireworkCenter,int delay) {
  int numVertices = (*fireworkMesh).getNumVertices();

  Cvec3 color = Cvec3(rand()%255 / 255.,rand()%255 / 255.,rand()%255 / 255.);
  
  for (int i = 0 ; i < numVertices; i++)
  {
    Mesh::Vertex v = (*fireworkMesh).getVertex(i);
    g_fireworkParticles.push_back(Particle(fireworkCenter + v.getPosition()*0.35,v.getNormal()*0.15,color,rand() % 20 + 35,delay));
    g_fireworkParticles.push_back(Particle(fireworkCenter + v.getPosition()*0.3,v.getNormal()*0.15,color,rand() % 20 + 35,delay));
    g_fireworkParticles.push_back(Particle(fireworkCenter + v.getPosition()*0.2,v.getNormal()*0.15,color,rand() % 20 + 35,delay));
    g_fireworkParticles.push_back(Particle(fireworkCenter + v.getPosition()*0.1,v.getNormal()*0.15,color,rand() % 20 + 35,delay));
    g_fireworkParticles.push_back(Particle(fireworkCenter + v.getPosition()*0.05,v.getNormal()*0.15,color,rand() % 20 + 35,delay));
  }
  //return;
}

static void animateAllFireworks(int ms) {
  float t = (float) ms / (float) g_msBetweenKeyFrames;
  
  bool dead = g_fireworkParticles.empty();

  vector<VertexPN> vs;

  if (!dead) {
    //adjust all the particles
    for (list<Particle>::iterator it = g_fireworkParticles.begin(), 
	   end = g_fireworkParticles.end(); it != end; it++) {
      if ((*it).dead() ) {
	list<Particle>::iterator it2 = it;
	it2++;
	g_fireworkParticles.erase(it);
	it = it2;
      }
      else
      {
	(*it).update();
	vs.push_back(VertexPN(it->getPosition(),it->getColor()));
      }
    }

    g_mainFirework->reset(&vs[0], vs.size());
    
    glutPostRedisplay();

    glutTimerFunc(1000/g_animateFramesPerSecond,
		  animateAllFireworks,
		  ms+1000/g_animateFramesPerSecond);
  }
}

static void animateTimerCallback (int ms) {
  glutPostRedisplay();
  glutTimerFunc(1000/g_animateFramesPerSecond,
		animateTimerCallback, ms+1000/g_animateFramesPerSecond);
}

static Cvec3 getScreenSpaceCoords(const Cvec3& center, double radius,            // camera/eye coordinate info for sphere
                                 const Matrix4& projection,                     // projection matrix
                                 const double frustNear, const double frustFovY,
                                 const int screenWidth, const int screenHeight){//, // viewport size
                                 //Cvec3& out) {         // output data in screen coordinates
  // returns false if the arcball is behind the viewer
  // get post projection canonical coordinates
  Cvec3 postproj = Cvec3(projection * Cvec4(center, 1));

  postproj[0] = postproj[0] * (double)screenWidth/2.0 + ((double)screenWidth-1.0)/2.0;
  postproj[1] = postproj[1] * (double)screenHeight/2.0 + ((double)screenHeight-1.0)/2.0;
  postproj[2] = postproj[2] * (double)screenHeight/2.0 + ((double)screenHeight-1.0)/2.0;

  return postproj;
}

static void updateFireworks(int x, int y) {

  Matrix4 projmat = Matrix4::makeProjection(
    g_frustFovY, g_windowWidth / static_cast <double> (g_windowHeight),
    g_frustNear, g_frustFar);

  Matrix4 invEyeMatrix = Matrix4();
  invEyeMatrix(2,3) = g_cameraDistance * -1;

  float x_eye = g_cameraDistance*2*((projmat(0,1)-1)*g_windowWidth + 2*x + 1) * (1/(projmat(0,0)*g_windowWidth));
  float y_eye = g_cameraDistance*2*(projmat(1,2)*g_windowHeight - g_windowHeight + 2*y + 1) * (1/(projmat(1,1)*g_windowHeight));

  if (y_eye < g_groundY) {
    cout << "Don't burn the ground, silly!" << endl;
    return;
  }

  Cvec4 eye_coords = Cvec4(x_eye,y_eye,0,1);

  Cvec4 world_coords = invEyeMatrix * eye_coords;

  Cvec3 translate = Cvec3(world_coords);
  
  if (g_currentFirework == 1) {
    initFirework(&g_firework1Mesh,translate,0);
    animateAllFireworks(0);
  }
  else if (g_currentFirework == 2) {
    initFirework(&g_firework2Mesh,translate,0);
    animateAllFireworks(0);
  }
}


static void mouse(const int button, const int state, const int x, const int y) {
  g_mouseClickX = x;
  g_mouseClickY = g_windowHeight - y - 1;  // conversion from GLUT window-coordinate-system to OpenGL window-coordinate-system

  g_mouseLClickButton |= (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN);
  g_mouseRClickButton |= (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN);
  g_mouseMClickButton |= (button == GLUT_MIDDLE_BUTTON && state == GLUT_DOWN);

  g_mouseLClickButton &= !(button == GLUT_LEFT_BUTTON && state == GLUT_UP);
  g_mouseRClickButton &= !(button == GLUT_RIGHT_BUTTON && state == GLUT_UP);
  g_mouseMClickButton &= !(button == GLUT_MIDDLE_BUTTON && state == GLUT_UP);

  g_mouseClickDown = g_mouseLClickButton || g_mouseRClickButton || g_mouseMClickButton;

  if (g_mouseClickDown) {
    updateFireworks(g_mouseClickX, g_mouseClickY);
  }
}


static Cvec3 randTrans() {
  Matrix4 projmat = Matrix4::makeProjection(
    g_frustFovY, g_windowWidth / static_cast <double> (g_windowHeight),
    g_frustNear, g_frustFar);

  Matrix4 invEyeMatrix = Matrix4();
  invEyeMatrix(2,3) = g_cameraDistance * -1;

  float x_max = g_cameraDistance*2*((projmat(0,1)-1)*g_windowWidth + 2*g_windowWidth + 1) * (1/(projmat(0,0)*g_windowWidth));
  float y_max = g_cameraDistance*2*(projmat(1,2)*g_windowHeight - g_windowHeight + 2*g_windowHeight - 1) * (1/(projmat(1,1)*g_windowHeight));

  float x_min = g_cameraDistance*2*((projmat(0,1)-1)*g_windowWidth + 1) * (1/(projmat(0,0)*g_windowWidth));
  float y_min = g_groundY + 2;//g_cameraDistance*2*(projmat(1,2)*g_windowHeight - g_windowHeight) * (1/(projmat(1,1)*g_windowHeight));

  int modx = x_max - x_min;
  int mody = y_max - y_min;

  float x_eye = rand() % modx + x_min;
  float y_eye = rand() % mody + y_min;
  float z_eye = rand() % (int) g_cameraDistance;

  Cvec4 eye_coords = Cvec4(x_eye,y_eye,z_eye,1);

  Cvec4 world_coords = invEyeMatrix * eye_coords;

  return Cvec3(world_coords);
}

static void makeShape(int shape) {
  int x_scale = g_windowWidth / 16;
  int y_scale = g_windowHeight / 32;

  int thirdHeight = g_windowHeight / 3;

  Matrix4 projmat = Matrix4::makeProjection(
    g_frustFovY, g_windowWidth / static_cast <double> (g_windowHeight),
    g_frustNear, g_frustFar);

  Matrix4 invEyeMatrix = Matrix4();
  invEyeMatrix(2,3) = g_cameraDistance * -1;

  int pattern[50];
  int size;

  if (shape == 'g') {
    size = 50;
    int array[] = {2,14,  2,12,  2,10,  2,8,  2,6,  2,4,  
		     5,14,  7,14,  9,14,  9,12,  9,10,  9,8,  9,6,  9,4,
		     15,14, 13,14, 11,14, 11,12,  11,10,  13,10,  14,9,  15,7,  14,5,  13,4,  11,4};
    for (int i = 0; i < size; i++)
      pattern[i] = array[i];
  }
  else if (shape == 'm') {
    size = 26;
    int array[] = {4,4,4,6,4,8,4,12,4,14,6,12,8,10,10,12,12,14,12,12,12,8,12,6,12,4};
    for (int i = 0; i < size; i++)
      pattern[i] = array[i];
  }
  else if (shape == 'e') {
    size = 30;
    int array[] = {5,4,5,6,5,8,5,10,5,12,5,14,5,16, 7,16,9,16,11,16, 7,10,9,10, 7,4,9,4,11,4};
    for (int i = 0; i < size; i++)
      pattern[i] = array[i];
  }
  else return;

  for (int i = 0; i < size; i++) {
    float x_eye = 
      g_cameraDistance*2*((projmat(0,1)-1)*g_windowWidth + 2*(pattern[i]*x_scale) + 1) * 
      (1/(projmat(0,0)*g_windowWidth));
    i++;
    float y_eye = 
      g_cameraDistance*2*(projmat(1,2)*g_windowHeight - g_windowHeight + 2*(pattern[i]*y_scale+thirdHeight) + 1) * 
      (1/(projmat(1,1)*g_windowHeight));
    cout << pattern[i] << endl;

    Cvec4 eye_coords = Cvec4(x_eye,y_eye,0,1);

    Cvec4 world_coords = invEyeMatrix * eye_coords;

    Cvec3 translate = Cvec3(world_coords);

    initFirework(&g_firework2Mesh, translate, 0);
  }

  animateAllFireworks(0);
}

static void keyboard(const unsigned char key, const int x, const int y) {
  switch (key) {
  case 27:
    exit(0);                                  // ESC
  case 'h':
    cout << " ============== H E L P ==============\n\n"
    << "h\t\thelp menu\n"
    << "s\t\tsave screenshot\n"
    << "1\t\tFirework #1\n"
    << "2\t\tFirework #2\n" 
    << "*\t\tSet off firework show\n" << endl;
    break;
  case 's':
    glFlush();
    writePpmScreenshot(g_windowWidth, g_windowHeight, "out.ppm");
    break;
  case '1':
    g_currentFirework = 1;
    cout << "Firework #1" << endl;
    break;
  case '2':
    g_currentFirework = 2;
    cout << "Firework #2" << endl;
    break;
 case '*':
   for (int i = 0; i < 8; i++) {
     initFirework(&g_firework1Mesh, randTrans(), rand()%300);
     initFirework(&g_firework1Mesh, randTrans(), rand()%300);
     initFirework(&g_firework2Mesh, randTrans(), rand()%300);
     initFirework(&g_firework2Mesh, randTrans(), rand()%300);
     initFirework(&g_firework2Mesh, randTrans(), rand()%300);
     animateAllFireworks(0);
   }
   break;
  case 'g':
  case 'm':
  case 'e':
   makeShape(key);
   break;
  }

  glutPostRedisplay();
}

static void initGlutState(int argc, char * argv[]) {
  glutInit(&argc, argv);                                  // initialize Glut based on cmd-line args
  glutInitDisplayMode(GLUT_RGBA|GLUT_DOUBLE|GLUT_DEPTH);  //  RGBA pixel channels and double buffering
  glutInitWindowSize(g_windowWidth, g_windowHeight);      // create a window
  glutCreateWindow("Final Project");                       // title the window

  glutDisplayFunc(display);                               // display rendering callback
  glutReshapeFunc(reshape);                               // window reshape callback
  glutMouseFunc(mouse);
  glutKeyboardFunc(keyboard);
}

static void initGLState() {
  glClearColor(0., 0., 0., 0.);
  glClearDepth(0.);
  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  glPixelStorei(GL_PACK_ALIGNMENT, 1);
  glCullFace(GL_BACK);
  glEnable(GL_CULL_FACE);
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_GREATER);
  glReadBuffer(GL_BACK);
  if (!g_Gl2Compatible)
    glEnable(GL_FRAMEBUFFER_SRGB);
}

static void initMaterials() {
  // Create some prototype materials
  Material diffuse("./shaders/basic-gl3.vshader", "./shaders/diffuse-gl3.fshader");
  Material solid("./shaders/basic-gl3.vshader", "./shaders/solid-gl3.fshader");
  Material specular("./shaders/basic-gl3.vshader", "./shaders/specular-gl3.fshader");
  Material firework("./shaders/firework-gl3.vshader", "./shaders/firework-gl3.fshader");

  // copy diffuse prototype and set blue color
  g_greenDiffuseMat.reset(new Material(diffuse));
  g_greenDiffuseMat->getUniforms().put("uColor", Cvec3f(0.0, 0.4, 0.086));

  g_fireworkMat.reset(new Material(firework));
  
  // copy solid prototype, and set to color white
  g_lightMat.reset(new Material(solid));
  g_lightMat->getUniforms().put("uColor", Cvec3f(1, 1, 1));

};

static void initGeometry() {
  initGround();
  initSpheres();
  initLights();
  initMesh();
  g_mainFirework.reset(new ParticleGeometryPN(0));
}

static void initScene() {
  g_world.reset(new SgRootNode());

  g_skyNode.reset(new SgRbtNode(RigTForm(Cvec3(0.0,0.0,g_cameraDistance))));

  g_groundNode.reset(new SgRbtNode());
  g_groundNode->addChild(shared_ptr<SgGeometryShapeNode>(
							 new SgGeometryShapeNode(g_ground, g_greenDiffuseMat, Cvec3(0, g_groundY, 0))));

  // defining subDiv node
  g_world->addChild(g_skyNode);
  g_world->addChild(g_groundNode);

  g_fireworkNode.reset(new SgRbtNode());
  g_fireworkNode->addChild(shared_ptr<SgGeometryShapeNode>(new SgGeometryShapeNode(g_mainFirework, g_fireworkMat, Cvec3(0,0,0))));
  
  // add light
  g_lightNode.reset(new SgRbtNode(RigTForm(Cvec3(5, 5, -1))));

  g_world->addChild(g_lightNode);
  g_world->addChild(g_fireworkNode);
  g_lightNode->addChild(shared_ptr<SgGeometryShapeNode>(new SgGeometryShapeNode(g_light, g_lightMat, Cvec3(0, 0, 0))));
}

int main(int argc, char * argv[]) {
  try {
    initGlutState(argc,argv);

    glewInit(); // load the OpenGL extensions

    cout << (g_Gl2Compatible ? "Will use OpenGL 2.x / GLSL 1.0" : "Will use OpenGL 3.x / GLSL 1.3") << endl;
    if ((!g_Gl2Compatible) && !GLEW_VERSION_3_0)
      throw runtime_error("Error: card/driver does not support OpenGL Shading Language v1.3");
    else if (g_Gl2Compatible && !GLEW_VERSION_2_0)
      throw runtime_error("Error: card/driver does not support OpenGL Shading Language v1.0");

    initGLState();
    initMaterials();
    initGeometry();
    initScene();
    animateAllFireworks(0);
    glutMainLoop();
    return 0;
  }
  catch (const runtime_error& e) {
    cout << "Exception caught: " << e.what() << endl;
    return -1;
  }
}
