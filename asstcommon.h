#ifndef ASSTCOMMON_H
#define ASSTCOMMON_H

#include <cstddef>
#include <vector>
#include <memory>
#include <stdexcept>
#if __GNUG__
#   include <tr1/memory>
#endif

#include "ppm.h"
#include "geometry.h"
#include "glsupport.h"
#include "scenegraph.h"
#include "uniforms.h"
#include "material.h"

extern const bool g_Gl2Compatible;

// takes a projection matrix and send to the the shaders
inline void sendProjectionMatrix(Uniforms& uniforms, const Matrix4& projMatrix) {
  uniforms.put("uProjMatrix", projMatrix);
}

// takes MVM and its normal matrix to the shaders
inline void sendModelViewNormalMatrix(Uniforms& uniforms, const Matrix4& MVM, const Matrix4& NMVM) {
  uniforms.put("uModelViewMatrix", MVM).put("uNormalMatrix", NMVM);
}


//---------------------------------------------------
// Scene graph shape node that references a geometry
//---------------------------------------------------

extern std::tr1::shared_ptr<Material> g_overridingMaterial;

class SgGeometryShapeNode : public SgShapeNode {
public:
  std::tr1::shared_ptr<Geometry> geometry;
  std::tr1::shared_ptr<Material> material;
  Matrix4 affineMatrix;

  SgGeometryShapeNode(std::tr1::shared_ptr<Geometry> _geometry,
                      std::tr1::shared_ptr<Material> _material,
                      const Cvec3& translation = Cvec3(0, 0, 0),
                      const Cvec3& eulerAngles = Cvec3(0, 0, 0),
                      const Cvec3& scales = Cvec3(1, 1, 1))
    : geometry(_geometry)
    , material(_material)
    , affineMatrix(Matrix4::makeTranslation(translation) *
                   Matrix4::makeXRotation(eulerAngles[0]) *
                   Matrix4::makeYRotation(eulerAngles[1]) *
                   Matrix4::makeZRotation(eulerAngles[2]) *
                   Matrix4::makeScale(scales)) {}

  virtual Matrix4 getAffineMatrix() {
    return affineMatrix;
  }

  void setAffineMatrix(const Cvec3& translation = Cvec3(0, 0, 0),
                       const Cvec3& eulerAngles = Cvec3(0, 0, 0),
                       const Cvec3& scales = Cvec3(1, 1, 1)) {
    affineMatrix = Matrix4::makeTranslation(translation) *
                   Matrix4::makeXRotation(eulerAngles[0]) *
                   Matrix4::makeYRotation(eulerAngles[1]) *
                   Matrix4::makeZRotation(eulerAngles[2]) *
                   Matrix4::makeScale(scales);
  }

  virtual void draw(const Uniforms& uniforms) {
    if (g_overridingMaterial)
      g_overridingMaterial->draw(*geometry, uniforms);
    else
      material->draw(*geometry, uniforms);
  }
};



#endif