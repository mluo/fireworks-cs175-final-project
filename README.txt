Michelle Luo and Megan Quintero CS175: Computer Graphics
Final Project 2011
Project Writeup and Details

We are submitting the following files:
• arcball.h, asstcommon.h, cube.mesh, cvec.h, drawer.h,
geometry.cpp, geometry.h, geometry.o, geometrymaker.h,
glsupport.cpp, glsupport.o, Makefile, material.cpp, material.h, matrix4.h, mesh.h, mesh.interface, particle.h, picker.cpp,
picker.h, picker.o, ppm.cpp, ppm.h, ppm.o, project, project.cpp,
project.o, project.sln, project.sln, project.vcxproj, project.vcxproj.filters, quat.h, renderstate.cpp, renderstate.h, renderstate.o, rigtform.h, scenegraph.cpp, scenegraph.h, scenegraph.o, sgutils.h, texture.cpp, texture.h, texture.o, and uniforms.h.

In addition to these files, we also submitted the following shaders within the shader folder:
• basic-gl2.vshader, basic-gl3.vshader, diffuse-gl2.fshader, diffuse-gl3.fshader, firework-gl2.fshader, firework-gl2.vshader,
firework-gl3.fshader, firework-gl3.vshader, normal-gl2.fshader, normal-gl2.vshader, normal-gl3.fshader, normal-gl3.vshader,
solid-gl2.fshader, solid-gl3.fshader, specular-gl2.fshader, and specular-gl3.fshader.
Platform: Mac OSX Snow Leopard.
Compiling Instructions: Our program can be run and compiled in the standard way. Our Makefile resembles the typical Makefile we used
throughout the semester. We would recommend that optimization compiling is used.

The following files are from a combination of pset work and the original code from the CS175 staff:
• asstcommon.h, cvec.h, drawer.h, geometrymaker.h, glsupport.cpp, glsupport.h, material.cpp, material.h, matrix4.h, mesh.h, mesh.interface, ppm.h, quat.h, renderstates.cpp, renderstates.h, rigtform.h, scenegraph.cpp, scenegraph.h, sgutils.h, texture.cpp, texture.h, and uniforms.h.
The following files are variations of code from the psets and the CS175 staff (a description of how we updated the files will follow):

• geometry.cpp, geometry.h, and the basic, diffuse, normal, and solid shaders.

~~~

Shaders:
o We had to adjust the shaders to deal with only one light
and to use light that was most appropriate to our needs.

Geometry.cpp, geometry.h:
o We created a new class named ParticleGeometryPN that is dynamic geometry object that uses VertexPNs. Instead of
drawing the attributes as triangles (as is the case with the other geometries), a ParticleGeometryPN draws using
points which is essential to our particle system. We also
enabled blending to blend two firework going off at once that overlap. This produces the fire-like blur of the fireworks. Within geometry.h, we also adjusted the way in which the attribute description are displayed. We concluded that we will not need the normal and instead intake a color and store that and send that to the vertex shader .
The original files that we added are the following:
• project.cpp, particle.h and the firework shaders. Following is a
brief description of how each file works and why it is important
to our project's design.

Shaders:
o The firework vertex shader is passed "aPosition" and
"aColor" and the vertex color is set to "aColor". We use "aPosition" to set the vertex position similarly to how we do so in the shaders given to us by the staff. The firework fragment shader sets the gl_FragColor to the varying color attribute.
Particle.h:
o A particle is a class that contains the following elements:
position, velocity, acceleration, color, lifetime, and delay. The particle class contains a function, "update" that updates the velocity (multiplied by a factor of 0.9) based on the decreasing acceleration. The position after the velocity is then updated as well and the lifetime is decreased. This accounts for the particles slowly down and staying in the air for longer after the firework has been set up to make the firework look more realistic. For lifetime element accounts for the time in which it takes a firework to fade out. The firework dies after a life time of whatever value it has been set to. A particle also has the component, delay. When you are simply clicking on a spot

on the screen for one firework to set off, then the delay is 0, so the firework goes off immediately. However, for our "surprise", we tried to make the fireworks look more like a firework show with the different fireworks being set off at different times.
Project.cpp:
o This is our main file that incorporates all of our code. We
based this file off our the asst7.cpp file very loosely and incorporated the idea of using meshes for our fireworks. I will discuss this file by describing the important functions.
o initMesh():
  This function initializes the meshes for the 4
    corresponding firework meshes. We created the different types of firework meshes by subdividing the
    cube mesh different numbers of times. o initFirework():
  We use this function to place the particles into vectors of "particles".
o animateAllFireworks():
  We originally started out with a function for each of
    the fireworks, however, we were able to integrate it
    into one function. We use vertex iterators to
    animate the particle system. o animateTimerCallback():
  We created this function to call glutPostRedisplay()
    in the background to not slow down the firework
    rendering.
o getScreenSpaceCoords():
  We created this function to convert mouse coordinates to world coordinates to allow the user to
    click where they want the firework to go off from. o updateFireworks():
  We created this function to reinitialize the particle system of the fireworks. We make sure to do the correct conversions between world and eye coordinates and to check to see if the user has selected the first or second type of firework.
o randTrans():
  We created this function to use in our "surprise" to
    generate a random world coordinate to center the
    fireworks. o Keyboard():
  Within keyboard, we added the hot keys of 1 and 2 to toggle between the two types of fireworks. The

~~~

To use:
Compile using 'make OPT=1' using './project'
Clicking anywhere on the screen will immediately set off a firework there, but be careful not to burn the ground!
Hit 'h' to pull up help menu. Main hot keys are:
  1: Change the active firework to type 1. 
  2: Change the active firework to type 2. 
  *: Wait for a surprise!
The window can be re-sized by dragging the corner, but the camera, the ground, and the moon are fixed.

We hope you enjoyed our project!