#include <stdexcept>
#include <string>

#include "geometry.h"

using namespace std;

GeometryPN::GeometryPN(const VertexPN *vtx, const unsigned short *idx, int vboLen, int iboLen) {
  this->vboLen = vboLen;
  this->iboLen = iboLen;

  // Now create the VBO and IBO
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(VertexPN) * vboLen, vtx, GL_STATIC_DRAW);

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned short) * iboLen, idx, GL_STATIC_DRAW);
}

void GeometryPN::draw(int attribIndices[]) {
  // bind vbo
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  safe_glVertexAttribPointer(attribIndices[0], 3, GL_FLOAT, GL_FALSE, sizeof(VertexPN), FIELD_OFFSET(VertexPN, p));
  safe_glVertexAttribPointer(attribIndices[1], 3, GL_FLOAT, GL_FALSE, sizeof(VertexPN), FIELD_OFFSET(VertexPN, n));

  // bind ibo
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);

  // draw!
  glDrawElements(GL_TRIANGLES, iboLen, GL_UNSIGNED_SHORT, 0);
}


GeometryPNTBX::GeometryPNTBX(const VertexPNTBX *vtx, const unsigned short *idx, int vboLen, int iboLen) {
  this->vboLen = vboLen;
  this->iboLen = iboLen;

  // Now create the VBO and IBO
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(VertexPNTBX) * vboLen, vtx, GL_STATIC_DRAW);

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned short) * iboLen, idx, GL_STATIC_DRAW);
}

void GeometryPNTBX::draw(int attribIndices[]) {
  // bind vbo
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  safe_glVertexAttribPointer(attribIndices[0], 3, GL_FLOAT, GL_FALSE, sizeof(VertexPNTBX), FIELD_OFFSET(VertexPNTBX, p));
  safe_glVertexAttribPointer(attribIndices[1], 3, GL_FLOAT, GL_FALSE, sizeof(VertexPNTBX), FIELD_OFFSET(VertexPNTBX, n));
  safe_glVertexAttribPointer(attribIndices[2], 3, GL_FLOAT, GL_FALSE, sizeof(VertexPNTBX), FIELD_OFFSET(VertexPNTBX, t));
  safe_glVertexAttribPointer(attribIndices[3], 3, GL_FLOAT, GL_FALSE, sizeof(VertexPNTBX), FIELD_OFFSET(VertexPNTBX, b));
  safe_glVertexAttribPointer(attribIndices[4], 2, GL_FLOAT, GL_FALSE, sizeof(VertexPNTBX), FIELD_OFFSET(VertexPNTBX, x));

  // bind ibo
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);

  // draw!
  glDrawElements(GL_TRIANGLES, iboLen, GL_UNSIGNED_SHORT, 0);
}


NonIndexedDynamicGeometryPN::NonIndexedDynamicGeometryPN(int initialLen) {
  this->vboLen = 0;
  this->allocVboLen = initialLen;

  // Now create the VBO
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(VertexPN) * initialLen, NULL, GL_DYNAMIC_DRAW);
  checkGlErrors();
}

void NonIndexedDynamicGeometryPN::reset(const VertexPN *vtx, int vboLen) {
  if (vboLen > this->allocVboLen) {
    this->allocVboLen = vboLen;
    cerr << "Dynamic VBO alloc size changed to " << vboLen << endl;
  }
  this->vboLen = vboLen;

  glBindBuffer(GL_ARRAY_BUFFER, vbo);

  // We always call glBufferData with a NULL ptr here, so that OpenGL knows that
  // we're done with old data, and that if the old vbo is in use, it can  allocate
  // us a new one without stalling the pipeline
  glBufferData(GL_ARRAY_BUFFER, sizeof(VertexPN) * allocVboLen, NULL, GL_DYNAMIC_DRAW);

  // now feed in the data
  glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(VertexPN) * vboLen, vtx);
  checkGlErrors();
}

void NonIndexedDynamicGeometryPN::draw(int attribIndices[]) {
  // bind vbo
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  safe_glVertexAttribPointer(attribIndices[0], 3, GL_FLOAT, GL_FALSE, sizeof(VertexPN), FIELD_OFFSET(VertexPN, p));
  safe_glVertexAttribPointer(attribIndices[1], 3, GL_FLOAT, GL_FALSE, sizeof(VertexPN), FIELD_OFFSET(VertexPN, n));

  // draw without using ibo!
  glDrawArrays(GL_TRIANGLES, 0, vboLen);
}

ParticleGeometryPN::ParticleGeometryPN(int initialLen) {
  this->vboLen = 0;
  this->allocVboLen = initialLen;

  // Now create the VBO
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(VertexPN) * initialLen, NULL, GL_DYNAMIC_DRAW);
  checkGlErrors();
}

void ParticleGeometryPN::reset(const VertexPN *vtx, int vboLen) {
  if (vboLen > this->allocVboLen) {
    this->allocVboLen = vboLen;
    //cerr << "Dynamic VBO alloc size changed to " << vboLen << endl;
  }
  this->vboLen = vboLen;

  glBindBuffer(GL_ARRAY_BUFFER, vbo);

  // We always call glBufferData with a NULL ptr here, so that OpenGL knows that
  // we're done with old data, and that if the old vbo is in use, it can  allocate
  // us a new one without stalling the pipeline
  glBufferData(GL_ARRAY_BUFFER, sizeof(VertexPN) * allocVboLen, NULL, GL_DYNAMIC_DRAW);

  // now feed in the data
  glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(VertexPN) * vboLen, vtx);
  checkGlErrors();
}

void ParticleGeometryPN::draw(int attribIndices[]) {
  // bind vbo
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  safe_glVertexAttribPointer(attribIndices[0], 3, GL_FLOAT, GL_FALSE, sizeof(VertexPN), FIELD_OFFSET(VertexPN, p));
  safe_glVertexAttribPointer(attribIndices[1], 3, GL_FLOAT, GL_FALSE, sizeof(VertexPN), FIELD_OFFSET(VertexPN, n));

  glEnable(GL_BLEND);
  glDepthMask(GL_FALSE);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE);

  // draw without using ibo!
  glDrawArrays(GL_POINTS, 0, vboLen);

  glDisable(GL_BLEND);
  glDepthMask(GL_TRUE);
}


NonIndexedDynamicGeometryPNX::NonIndexedDynamicGeometryPNX(int initialLen) {
  this->vboLen = 0;
  this->allocVboLen = initialLen;

  // Now create the VBO
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(VertexPNX) * initialLen, NULL, GL_DYNAMIC_DRAW);
  checkGlErrors();
}

void NonIndexedDynamicGeometryPNX::reset(const VertexPNX *vtx, int vboLen) {
  if (vboLen > this->allocVboLen) {
    this->allocVboLen = vboLen;
    cerr << "Dynamic VBO alloc size changed to " << vboLen << endl;
  }
  this->vboLen = vboLen;

  glBindBuffer(GL_ARRAY_BUFFER, vbo);

  // We always call glBufferData with a NULL ptr here, so that OpenGL knows that
  // we're done with old data, and that if the old vbo is in use, it can  allocate
  // us a new one without stalling the pipeline
  glBufferData(GL_ARRAY_BUFFER, sizeof(VertexPNX) * allocVboLen, NULL, GL_DYNAMIC_DRAW);

  // now feed in the data
  glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(VertexPNX) * vboLen, vtx);
  checkGlErrors();
}

void NonIndexedDynamicGeometryPNX::draw(int attribIndices[]) {
  // bind vbo
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  safe_glVertexAttribPointer(attribIndices[0], 3, GL_FLOAT, GL_FALSE, sizeof(VertexPNX), FIELD_OFFSET(VertexPNX, p));
  safe_glVertexAttribPointer(attribIndices[1], 3, GL_FLOAT, GL_FALSE, sizeof(VertexPNX), FIELD_OFFSET(VertexPNX, n));
  safe_glVertexAttribPointer(attribIndices[2], 2, GL_FLOAT, GL_FALSE, sizeof(VertexPNX), FIELD_OFFSET(VertexPNX, x));

  // draw without using ibo!
  glDrawArrays(GL_TRIANGLES, 0, vboLen);
}
