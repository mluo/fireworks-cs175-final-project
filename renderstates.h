#ifndef RENDERSTATES_H
#define RENDERSTATES_H

#include "glsupport.h"

// Currently the following GL states are supported:
// - polygon mode
// - blend

class RenderStates {
  GLenum glFront, glBack;
  unsigned int flags;

public:
  RenderStates();

  RenderStates& polygonMode(GLenum face, GLenum mode);
  RenderStates& enable(GLenum target);
  RenderStates& disable(GLenum target);

  void apply() const;
  void captureFromGl();
};

#endif