#include <stdexcept>

#include "glsupport.h"
#include "renderstates.h"

using namespace std;

RenderStates::RenderStates()
  : glFront(GL_FILL)
  , glBack(GL_FILL)
  , flags(0) {}

RenderStates& RenderStates::polygonMode(GLenum face, GLenum mode) {
  switch (mode) {
  case GL_POINT:
  case GL_LINE:
  case GL_FILL:
    switch (face) {
    case GL_FRONT_AND_BACK:
      glFront = glBack = mode;
      return *this;
    case GL_FRONT:
      glFront = mode;
      return *this;
    case GL_BACK:
      glBack = mode;
      return *this;
    default:
      ;
    }
  default:
    ;
  }

  throw invalid_argument("RenderStates::glPolygonMode: invalid argument");
}

static const unsigned int kBlendBit = 1;

RenderStates& RenderStates::enable(GLenum target) {
  switch (target) {
  case GL_BLEND:
    flags |= kBlendBit;
    return *this;
  default:
    ;
  }
  throw invalid_argument("RenderStates::glEnable: unsupported target");
}

RenderStates& RenderStates::disable(GLenum target) {
  switch (target) {
  case GL_BLEND:
    flags &= ~kBlendBit;
    return *this;
  default:
    ;
  }
  throw invalid_argument("RenderStates::glEnable: unsupported target");
}

void RenderStates::apply() const {
  static bool firstRun = false;
  static RenderStates currentRs;
  if (firstRun) {
    currentRs.captureFromGl();
    firstRun = false;
  }

  if (glFront != currentRs.glFront) {
    ::glPolygonMode(GL_FRONT, glFront);
    currentRs.glFront = glFront;
  }
  if (glBack != currentRs.glBack) {
    ::glPolygonMode(GL_FRONT, glFront);
    currentRs.glBack = glBack;
  }
  if ((flags & kBlendBit) != (currentRs.flags & kBlendBit)) {
    if (flags & kBlendBit)
      ::glEnable(GL_BLEND);
    else
      ::glDisable(GL_BLEND);
    currentRs.flags = (currentRs.flags & (~kBlendBit)) | (flags & kBlendBit);
  }
}

void RenderStates::captureFromGl() {
  GLint values[2];
  ::glGetIntegerv(GL_POLYGON_MODE, values);
  glFront = values[0];
  glBack = values[1];

  flags = 0;
  ::glGetIntegerv(GL_BLEND, values);
  if (values[0] == GL_TRUE)
    flags |= kBlendBit;
}


