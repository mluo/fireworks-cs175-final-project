uniform sampler2D uTexColor;
uniform sampler2D uTexNormal;

// lights in eye space
uniform vec3 uLight;

varying vec2 vTexCoord;
varying mat3 vNTMat;
varying vec3 vEyePos;

void main() {
  // TODO: replace the following line with loading of normal from uTexNormal
  //       transforming to eye space, and normalizing

  vec4 color = texture2D(uTexNormal, vTexCoord);
  vec3 n = color.xyz;
 
  n[0] = (n[0] * 2.0) - 1.0;
  n[1] = (n[1] * 2.0) - 1.0;
  n[2] = (n[2] * 2.0) - 1.0;
  vec3 normal = normalize(vNTMat * n);


  vec3 viewDir = normalize(-vEyePos);
  vec3 lightDir = normalize(uLight - vEyePos);

  float nDotL = dot(normal, lightDir);
  vec3 reflection = normalize( 2.0 * normal * nDotL - lightDir);
  float rDotV = max(0.0, dot(reflection, viewDir));
  float specular = pow(rDotV, 25.0) * 0.4;
  float diffuse = max(nDotL, 0.0);

  gl_FragColor = texture2D(uTexColor, vTexCoord) * (diffuse + specular);
}
