#version 130

varying vec3 uColor;

out vec4 fragColor;

void main() {
  fragColor = vec4(vColor, 1.0);
}
