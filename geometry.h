#ifndef GEOMETRY_H
#define GEOMETRY_H

#include <cstddef>
#include <vector>
#include <cmath>
#include <string>

#include "cvec.h"
#include "glsupport.h"
#include "geometrymaker.h"

// An abstract class that encapsulates OpenGL vertex buffer and index buffer objects
class Geometry {
public:
  // Descriptor of one stream of vertex attribute provided by the geometry
  struct AttribDesc {
    // Name of the attribute, as expected by GLSL shader
    std::string name;

    // Must be one of GL_FLOAT, GL_FLOAT_VEC2, GL_FLOAT_VEC3, GL_FLOAT_VEC4,
    // GL_INT, GL_INT_VEC2, GL_INT_VEC3, GL_INT_VEC4, GL_DOUBLE, GL_DOUBLE_VEC2,
    // GL_DOUBLE_VEC3, or GL_DOUBLE_VEC4
    //
    // Attribute specified by glVertexAttribPointer must correspond to GL_FLOAT*
    // Attribute specified by glVertexAttribPointerI must correspond to GL_INT*
    // Attribute specified by glVertexAttribPointerL must correspond to GL_DOUBLE*
    GLenum type;
  };

  // Return the number of vertex attribute streams provided by the geometry
  virtual int getNumAttribs() const = 0;

  // Return the start of the array of vertex attribute descriptors, one for each
  // vertex attribute stream
  virtual const AttribDesc* getAttribDesc() const = 0;

  // Draw the geometry. attribIndices[i] corresponds to the index of the
  // shader vertex attribute location that the i-th vertex attribute stream provided
  // by this geometry should bind to. It can be -1 to indicate that this stream is
  // not used. The caller is responsible for enable/disable vertex attribute arrays.
  virtual void draw(int attribIndices[]) = 0;

  virtual ~Geometry() {}
};

//---------------------------------------------------------------------------------
// Two concrete implementations of Geometry:
// - One with only position and normal streams.
// - One with position, normal, texture coordinates, binormal, and tangent;
// --------------------------------------------------------------------------------

#define FIELD_OFFSET(StructType, field) ((GLvoid*)(offsetof(StructType, field)))

// A vertex with floating point Position, and Normal;
struct VertexPN {
  Cvec3f p, n;

  VertexPN() {}

  VertexPN(float x, float y, float z,
           float nx, float ny, float nz)
    : p(x,y,z), n(nx, ny, nz) {}

  VertexPN(const Cvec3f& pos, const Cvec3f& normal)
    : p(pos), n(normal) {}

  VertexPN(const Cvec3& pos, const Cvec3& normal)
    : p(pos[0], pos[1], pos[2]), n(normal[0], normal[1], normal[2]) {}


  // Define copy constructor and assignment operator from GenericVertex so we can
  // use make* functions from geometrymaker.h
  VertexPN(const GenericVertex& v) {
    *this = v;
  }

  VertexPN& operator = (const GenericVertex& v) {
    p = v.pos;
    n = v.normal;
    return *this;
  }

};

// A vertex with floating point Position, Normal, and one set of teXture Coordinates;
struct VertexPNX : public VertexPN {
  Cvec2f x; // texture coordinates

  VertexPNX() {}

  VertexPNX(float x, float y, float z,
            float nx, float ny, float nz,
            float u, float v)
    : VertexPN(x, y, z, nx, ny, nz), x(u, v) {}

  VertexPNX(const Cvec3f& pos, const Cvec3f& normal, const Cvec2f& texCoords)
    : VertexPN(pos, normal), x(texCoords) {}

  VertexPNX(const Cvec3& pos, const Cvec3& normal, const Cvec2& texCoords)
    : VertexPN(pos, normal), x(texCoords[0], texCoords[1]) {}


  // Define copy constructor and assignment operator from GenericVertex so we can
  // use make* functions from geometrymaker.h
  VertexPNX(const GenericVertex& v) {
    *this = v;
  }

  VertexPNX& operator = (const GenericVertex& v) {
    p = v.pos;
    n = v.normal;
    x = v.tex;
    return *this;
  }

};


// A vertex with floating point Position, Normal, Tangent, Binormal, teXture Coord
struct VertexPNTBX : public VertexPNX {
  Cvec3f t, b; // tangent, binormal

  VertexPNTBX() {}

  VertexPNTBX(float x, float y, float z,
              float nx, float ny, float nz,
              float tx, float ty, float tz,
              float bx, float by, float bz,
              float u, float v)
    : VertexPNX(x, y, z, nx, ny, nz, u, v), t(tx, ty, tz), b(bx, by, bz) {}

  VertexPNTBX(const Cvec3f& pos, const Cvec3f& normal,
              const Cvec3f& tangent, const Cvec3f& binormal, const Cvec2f& texCoords)
    : VertexPNX(pos, normal, texCoords), t(tangent), b(binormal) {}

  VertexPNTBX(const Cvec3& pos, const Cvec3& normal,
              const Cvec3& tangent, const Cvec3& binormal, const Cvec2& texCoords)
    : VertexPNX(pos, normal, texCoords), t(tangent[0], tangent[1], tangent[2]), b(binormal[0], binormal[1], binormal[2]) {}

  // Define copy constructor and assignment operator from GenericVertex so we can
  // use make* functions from geometrymaker.h
  VertexPNTBX(const GenericVertex& v) {
    *this = v;
  }

  VertexPNTBX& operator = (const GenericVertex& v) {
    p = v.pos;
    n = v.normal;
    t = v.tangent;
    b = v.binormal;
    x = v.tex;
    return *this;
  }
};


// Future TODO: consolidate the following into one or two templated classes

// A Geometry that provides two vertex attribute streams:
// - aPosition: position
// - aNormal: normal
class GeometryPN : public Geometry {
  GlBufferObject vbo, ibo;
  int vboLen, iboLen;

public:
  GeometryPN(const VertexPN *vtx, const unsigned short *idx, int vboLen, int iboLen); // implemented in geometry.cpp

  static const int kNumAttribs = 2;

  virtual int getNumAttribs() const {
    return kNumAttribs;
  }

  virtual const AttribDesc* getAttribDesc() const {
    static const Geometry::AttribDesc attribDesc[kNumAttribs] = {
      {"aPosition", GL_FLOAT_VEC3},
      {"aNormal", GL_FLOAT_VEC3},
    };
    return attribDesc;
  }

  void reset(VertexPN *vtx, unsigned short *idx, int vboLen, int iboLen);

  virtual void draw(int attribIndices[]); // implemented in geometry.cpp
};


// A Geometry that provides five vertex attribute streams:
// - aPosition: position
// - aNormal: normal vector
// - aTangent: tangent vector
// - aBinormal: binormal vector
// - aTexCoord: texture coord

class GeometryPNTBX : public Geometry {
  GlBufferObject vbo, ibo;
  int vboLen, iboLen;

public:
  GeometryPNTBX(const VertexPNTBX *vtx, const unsigned short *idx, int vboLen, int iboLen); // implemented in geometry.cpp

  static const int kNumAttribs = 5;

  virtual int getNumAttribs() const {
    return kNumAttribs;
  }

  virtual const AttribDesc* getAttribDesc() const {
    static const Geometry::AttribDesc attribDesc[kNumAttribs] = {
      {"aPosition", GL_FLOAT_VEC3},
      {"aNormal", GL_FLOAT_VEC3},
      {"aTangent", GL_FLOAT_VEC3},
      {"aBinormal", GL_FLOAT_VEC3},
      {"aTexCoord", GL_FLOAT_VEC2}
    };

    return attribDesc;
  }

  virtual void draw(int attribIndices[]); // implemented in geometry.cpp
};


class NonIndexedDynamicGeometryPN : public Geometry {
  GlBufferObject vbo;
  int vboLen, allocVboLen;

public:
  NonIndexedDynamicGeometryPN(int initialVboLen);

  static const int kNumAttribs = 2;

  virtual int getNumAttribs() const {
    return kNumAttribs;
  }

  virtual const AttribDesc* getAttribDesc() const {
    static const Geometry::AttribDesc attribDesc[kNumAttribs] = {
      {"aPosition", GL_FLOAT_VEC3},
      {"aNormal", GL_FLOAT_VEC3},
    };
    return attribDesc;
  }

  void reset(const VertexPN *vtx, int vboLen);

  int getAllocatedVboLen() const { return allocVboLen; }

  virtual void draw(int attribIndices[]); // implemented in geometry.cpp
};

class ParticleGeometryPN : public Geometry {
  GlBufferObject vbo;
  int vboLen, allocVboLen;

public: 
  ParticleGeometryPN(int initialVboLen);

  static const int kNumAttribs = 2;

  virtual int getNumAttribs() const {
    return kNumAttribs;
  }
  
  virtual const AttribDesc* getAttribDesc() const {
    static const Geometry::AttribDesc attribDesc[kNumAttribs] = {
      {"aPosition", GL_FLOAT_VEC3},
      {"aColor", GL_FLOAT_VEC3},
    };
    return attribDesc;
  }

  void reset(const VertexPN *vtx, int vboLen);

  int getAllocatedVboLen() const { return allocVboLen; }

  virtual void draw(int attribIndices[]);
};

class NonIndexedDynamicGeometryPNX : public Geometry {
  GlBufferObject vbo;
  int vboLen, allocVboLen;

public:
  NonIndexedDynamicGeometryPNX(int initialVboLen);

  static const int kNumAttribs = 3;

  virtual int getNumAttribs() const {
    return kNumAttribs;
  }

  virtual const AttribDesc* getAttribDesc() const {
    static const Geometry::AttribDesc attribDesc[kNumAttribs] = {
      {"aPosition", GL_FLOAT_VEC3},
      {"aNormal", GL_FLOAT_VEC3},
      {"aTexCoord", GL_FLOAT_VEC2},
    };
    return attribDesc;
  }

  void reset(const VertexPNX *vtx, int vboLen);

  int getAllocatedVboLen() const { return allocVboLen; }

  virtual void draw(int attribIndices[]); // implemented in geometry.cpp
};




#endif
