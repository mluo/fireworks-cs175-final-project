#ifndef PARTICLE_H
#define PARTICLE_H

#include <cmath>

#include "cvec.h"
#include "geometry.h"

class Particle {
  Cvec3 position_, velocity_, acceleration_, color_;
  int lifetime_,delay_;

public:
Particle(Cvec3 position, Cvec3 velocity, Cvec3 color, int lifetime, int delay) {
    position_ = position;
    velocity_ = velocity;
    color_ = color;
    acceleration_ = 
      Cvec3( 0., -0.0005, 0);
    lifetime_ = lifetime; // age = 10s = 10000ms
    delay_ = delay;
  }

  Cvec3 getPosition() { return position_; }
  Cvec3 getColor() { 
    if (delay_ < 0)
      return color_; 
    else
      return Cvec3(0,0,0);
  }
  Cvec3 getVelocity() { return velocity_; }

  void update() {
    if (delay_ < 0) {
      velocity_ = velocity_ * 0.9 + acceleration_;
      position_ = position_ + velocity_;
      lifetime_ -= 1;
    }
    else
      delay_ -= 1;
  }
  
  bool dead() {
    return (lifetime_ <= 0.0);
  }

};



#endif
