////////////////////////////////////////////////////////////////////////
//
//   Harvard University
//   CS175 : Computer Graphics
//   Professor Steven Gortler
//
////////////////////////////////////////////////////////////////////////

#include <string>
#include <vector>
#include <list>
#include <memory>
#include <stdexcept>
#include <iostream>
#include <fstream>
#if __GNUG__
#   include <tr1/memory>
#endif

#include <GL/glew.h>
#ifdef __MAC__
#   include <GLUT/glut.h>
#else
#   include <GL/glut.h>
#endif

#include "arcball.h"
#include "asstcommon.h"
#include "cvec.h"
#include "drawer.h"
#include "geometry.h"
#include "glsupport.h"
#include "matrix4.h"
//#include "particle.h"
#include "picker.h"
#include "ppm.h"
#include "quat.h"
#include "rigtform.h"
#include "scenegraph.h"
#include "sgutils.h"
#include "geometrymaker.h"
#include "material.h"
#include "renderstates.h"
#include "texture.h"
#include "uniforms.h"
#include "mesh.h"
//#include "particle.h"


using namespace std;      // for string, vector, iostream, and other standard C++ stuff
using namespace std::tr1; // for shared_ptr

// G L O B A L S ///////////////////////////////////////////////////

// --------- IMPORTANT --------------------------------------------------------
// Before you start working on this assignment, set the following variable
// properly to indicate whether you want to use OpenGL 2.x with GLSL 1.0 or
// OpenGL 3.x+ with GLSL 1.3.
//
// Set g_Gl2Compatible = true to use GLSL 1.0 and g_Gl2Compatible = false to
// use GLSL 1.3. Make sure that your machine supports the version of GLSL you
// are using. In particular, on Mac OS X currently there is no way of using
// OpenGL 3.x with GLSL 1.3 when GLUT is used.
//
// If g_Gl2Compatible=true, shaders with -gl2 suffix will be loaded.
// If g_Gl2Compatible=false, shaders with -gl3 suffix will be loaded.
// To complete the assignment you only need to edit the shader files that get
// loaded
// ----------------------------------------------------------------------------
const bool g_Gl2Compatible = true;

static const float g_frustFovY = 60.0;    // 60 degree field of view in y direction
static const float g_frustNear = -0.1;    // near plane
static const float g_frustFar = -50.0;    // far plane
static const float g_groundY = -2.0;      // y coordinate of the ground
static const float g_groundSize = 10.0;   // half the ground length

static int g_numFiles = 0;

static int g_windowWidth = 512;
static int g_windowHeight = 512;
static bool g_mouseClickDown = false;    // is the mouse button pressed
static bool g_mouseLClickButton, g_mouseRClickButton, g_mouseMClickButton;
static bool g_zMotion = false; // keeps track of whether to use z-motion (hack for Macs)
static int g_mouseClickX, g_mouseClickY; // coordinates for mouse click event
static int g_activeShader = 0;
static bool g_worldSky = true;

bool g_smoothShading = false;


inline void print(const Matrix4& m) {
  for (int i = 0; i < 4; i++)
  {
    for (int j = 0; j < 4; j++)
      cout << m(i,j) << ", ";
    cout << "\n";
  }
}

static bool g_picking = false;

static shared_ptr<Material> //g_redDiffuseMat,
                            g_greenDiffuseMat,
                            g_bumpFloorMat,
                            g_arcballMat,
                            g_pickingMat,
			    g_lightMat,
			    g_sparkMat,
                            g_fireworkMat;

shared_ptr<Material> g_overridingMaterial;

// Vertex buffer and index buffer associated with the ground and cube geometry
static shared_ptr<Geometry> g_ground, g_cube, g_sphere, g_light, g_spark;
static shared_ptr<NonIndexedDynamicGeometryPN> g_firework1, g_firework2,
			g_firework3, g_firework4, g_firework5;
static Mesh g_m;
static Mesh g_firework1Mesh, g_firework2Mesh, g_firework3Mesh, g_firework4Mesh, g_firework5Mesh;
static int g_numSubdivisions = 0;


// --------- Scene

static const Cvec3 g_light1(2.0, 3.0, 14.0); 
static RigTForm g_skyRbt = RigTForm(Cvec3(0.0, 0.25, 4.0));

// RigTForm array to keep track of objects/eyes; last entry is sky

static RigTForm g_objectRbt[3] = 
  {RigTForm(Cvec3(-1,0,0)),
   RigTForm(Cvec3(1,0,0)),
   g_skyRbt};
static Cvec3f g_objectColors[2] = {Cvec3f(0.42, 0.28, 0.84),Cvec3f(0.047, 0.353, 0.651)};

// objects
static shared_ptr<SgRootNode> g_world;
static shared_ptr<SgRbtNode> g_skyNode, g_groundNode;//, g_robot1Node, g_robot2Node;
static shared_ptr<SgRbtNode> g_currentPickedRbtNode;
static shared_ptr<SgRbtNode> g_lightNode;//, g_light2Node;

static shared_ptr<SgRbtNode> g_firework1Node, g_firework2Node,
			g_firework3Node, g_firework4Node, g_firework5Node;
static shared_ptr<SgRbtNode> g_fireworksNode;

static RigTForm g_fireworkRbt[5] = {g_fireworkRbt[0] = RigTForm(Cvec3(-0.6, 0., -0.6)),
				    g_fireworkRbt[1] = RigTForm(Cvec3(-0.3, 0.5, -0.3)),
				    g_fireworkRbt[2] = RigTForm(Cvec3(0., 0., 0.)),
				    g_fireworkRbt[3] = RigTForm(Cvec3(0.3, 0.5, 0.3)),
				    g_fireworkRbt[4] = RigTForm(Cvec3(-0.6, 0, 0))}; 

// particle systems
static list<Particle> g_firework1Particles, g_firework2Particles, 
			g_firework3Particles, g_firework4Particles,
			g_firework5Particles;
static list<Particle> g_fireworkParticles;

static shared_ptr<NonIndexedDynamicGeometryParticle> g_fireworks;

// counters
static int g_currObject = 2; // keeps track of object to manipulate; default is sky
static int g_currEye = 2; //keeps track of which object is eye; default is sky
static int g_totalObjects = 3;
static const int g_skyIndex = g_totalObjects - 1; // defines the position of the sky in the object matrix array

// eye
static shared_ptr<SgRbtNode> g_eye;

static void setEye() 
{
  g_eye = g_skyNode;
  return;
}

// frames
static list<vector<RigTForm> > g_script;
static list<vector<RigTForm> >::iterator g_currFrame = g_script.begin();
static vector<shared_ptr<SgRbtNode> > g_nodes;

// timing/animation
static int g_msBetweenKeyFrames = 2000;       // 2 seconds between keyframes
static int g_animateFramesPerSecond = 60;     // frames to render per second during animation playback
static int g_animateMeshFramesPerSecond = 10; // frames to render per second for mesh animation
static bool g_playing = false;                // keep track of whether animation is playing

struct frameIndex {
  int frameNumber;
  list<vector<RigTForm> >::iterator frameIter;

  frameIndex(int n) {
    frameNumber = n;
    frameIter = g_script.begin();

    for (int i = -1; i < n; i++)
      frameIter++;
  }

  frameIndex& increment() {
    this->frameNumber += 1;
    (this->frameIter)++;
    return *this;
  }
};

frameIndex g_minus1 = frameIndex(-1);
frameIndex g_previous = frameIndex(0);
frameIndex g_next = frameIndex(1);
frameIndex g_plus1 = frameIndex(2);

// booleans for fireworkk mode
static bool g_f1active = false;
static bool g_f2active = false;
static bool g_f3active = false;
static bool g_f4active = false;
static bool g_f5active = false;
static int g_currentFirework = 1;

///////////////// END OF G L O B A L S //////////////////////////////////////////////////

static void initFireworkRbts() {
  g_fireworkRbt[0] = RigTForm(Cvec3(-0.6, 0., -0.6));
  g_fireworkRbt[1] = RigTForm(Cvec3(-0.3, 0.5, 0.3));
  g_fireworkRbt[2] = RigTForm(Cvec3(0., 0., 0.));
  g_fireworkRbt[3] = RigTForm(Cvec3(0.3, 0.5, 0.3));
  g_fireworkRbt[4] = RigTForm(Cvec3(-0.6, 0, -0.6));  
}


static void initGround() {
  int ibLen, vbLen;
  getPlaneVbIbLen(vbLen, ibLen);

  // Temporary storage for cube GeometryPNTBX
  vector<VertexPNTBX> vtx(vbLen);
  vector<unsigned short> idx(ibLen);

  makePlane(g_groundSize*2, vtx.begin(), idx.begin());
  g_ground.reset(new GeometryPNTBX(&vtx[0], &idx[0], vbLen, ibLen));
}

static void initCubes() {
  int ibLen, vbLen;
  getCubeVbIbLen(vbLen, ibLen);


  // Temporary storage for cube GeometryPNTBX
  vector<VertexPNTBX> vtx(vbLen);
  vector<unsigned short> idx(ibLen);

  makeCube(1, vtx.begin(), idx.begin());
  g_cube.reset(new GeometryPNTBX(&vtx[0], &idx[0], vbLen, ibLen));
}

// the centers of the fireworks are spheres
static void initSpheres() {
  int ibLen, vbLen;
  getSphereVbIbLen(20, 10, vbLen, ibLen);

  // Temporary storage for sphere GeometryPNTBX
  vector<VertexPNTBX> vtx(vbLen);
  vector<unsigned short> idx(ibLen);
  makeSphere(0.5, 20, 10, vtx.begin(), idx.begin());
  g_sphere.reset(new GeometryPNTBX(&vtx[0], &idx[0], vtx.size(), idx.size()));
}

static void dumpMesh(vector<VertexPN>* vs, Mesh* m, int numFaces) {
  for (int i = 0; i < numFaces; i++)
  {
    Mesh::Face face = (*m).getFace(i);

    bool quad = (face.getNumVertices() == 4);

    Cvec3 p1 = (face.getVertex(0).getPosition())*0.1;
    Cvec3 p2 = (face.getVertex(1).getPosition())*0.1;
    Cvec3 p3 = (face.getVertex(2).getPosition())*0.1;
    Cvec3 p4;
    if (quad)
      p4 = (face.getVertex(3).getPosition())*0.1;

    Cvec3 n1 = face.getNormal();
    Cvec3 n2 = n1;
    Cvec3 n3 = n1;
    Cvec3 n4 = n1;

    if (g_smoothShading)
    {
      Mesh::VertexIterator it1(face.getVertex(0).getIterator()), it1_0(it1);
      float nbrFaces = 0;
      n1 = Cvec3(0.,0.,0.);
      do
      {
	n1 = n1 + it1.getFace().getNormal();
	nbrFaces = nbrFaces + 1;
      } while (++it1 != it1_0);
      n1 = n1 / nbrFaces;
      n1.normalize();

      Mesh::VertexIterator it2(face.getVertex(1).getIterator()), it2_0(it2);
      nbrFaces = 0;
      n2 = Cvec3(0.,0.,0.);
      do
      {
	n2 = n2 + it2.getFace().getNormal();
	nbrFaces = nbrFaces + 1;
      } while (++it2 != it2_0);
      n2 = n2 / nbrFaces;
      n2.normalize();

      Mesh::VertexIterator it3(face.getVertex(2).getIterator()), it3_0(it3);
      nbrFaces = 0;
      n3 = Cvec3(0.,0.,0.);
      do
      {
	n3 = n3 + it3.getFace().getNormal();
	nbrFaces = nbrFaces + 1;
      } while (++it3 != it3_0);
      n3 = n3 / nbrFaces;
      n3.normalize();

      if (quad)
      {
	Mesh::VertexIterator it4(face.getVertex(3).getIterator()), it4_0(it4);
	nbrFaces = 0;
	n4 = Cvec3(0.,0.,0.);
	do
	{
	  n4 = n4 + it4.getFace().getNormal();
	nbrFaces = nbrFaces + 1;
	} while (++it4 != it4_0);
	n4 = n4 / nbrFaces;
	n4.normalize();
      }
    }

    VertexPN vpn1 = VertexPN(p1[0],p1[1],p1[2],n1[0],n1[1],n1[2]);
    VertexPN vpn2 = VertexPN(p2[0],p2[1],p2[2],n2[0],n2[1],n2[2]);
    VertexPN vpn3 = VertexPN(p3[0],p3[1],p3[2],n3[0],n3[1],n3[2]);
    VertexPN vpn4 = VertexPN(p4[0],p4[1],p4[2],n4[0],n4[1],n4[2]);

    face.getVertex(0).setNormal(n1);
    face.getVertex(1).setNormal(n2);
    face.getVertex(2).setNormal(n3);
    if (quad)
      face.getVertex(3).setNormal(n4);

    (*vs).push_back(vpn1);
    (*vs).push_back(vpn2);
    (*vs).push_back(vpn3);

    if (quad)
    {
      (*vs).push_back(vpn1);
      (*vs).push_back(vpn3);
      (*vs).push_back(vpn4);
    }
  }
}

static void initMesh() {
  g_m = Mesh();
  g_m.load("cube.mesh");

  g_firework1Mesh = g_m;

  for (int i=0; i<2; i++)
    g_firework1Mesh.subdivide();

  int numFaces = g_firework1Mesh.getNumFaces();
  vector<VertexPN> vs1;
  dumpMesh(&vs1, &g_firework1Mesh, numFaces);

  g_firework2Mesh = g_firework1Mesh;
  g_firework3Mesh = g_firework1Mesh;

  g_firework1.reset(new NonIndexedDynamicGeometryPN(vs1.size()));
  g_firework1->reset(&vs1[0], vs1.size());
  
  g_firework2.reset(new NonIndexedDynamicGeometryPN(vs1.size()));
  g_firework2->reset(&vs1[0], vs1.size());

  g_firework3.reset(new NonIndexedDynamicGeometryPN(vs1.size()));
  g_firework3->reset(&vs1[0], vs1.size());

  g_firework4Mesh = g_m;

  for (int i=0; i<4; i++)
    g_firework4Mesh.subdivide();

  numFaces = g_firework4Mesh.getNumFaces();
  vector<VertexPN> vs2;
  dumpMesh(&vs2, &g_firework4Mesh, numFaces);

  g_firework5Mesh = g_firework4Mesh;

  g_firework4.reset(new NonIndexedDynamicGeometryPN(vs2.size()));
  g_firework4->reset(&vs2[0], vs2.size());

  g_firework5.reset(new NonIndexedDynamicGeometryPN(vs2.size()));
  g_firework5->reset(&vs2[0], vs2.size());
}

static void initLights() {
  int ibLen, vbLen;
  getSphereVbIbLen(20, 10, vbLen, ibLen);

  // Temporary storage for sphere GeometryPNTBX
  vector<VertexPNTBX> vtx(vbLen);
  vector<unsigned short> idx(ibLen);
  makeSphere(0.2, 20, 10, vtx.begin(), idx.begin());
  g_light.reset(new GeometryPNTBX(&vtx[0], &idx[0], vtx.size(), idx.size()));
}

static void initSparks() {
  int ibLen, vbLen;
  getSphereVbIbLen(20, 10, vbLen, ibLen);

  // Temporary storage for sphere GeometryPNTBX
  vector<VertexPNTBX> vtx(vbLen);
  vector<unsigned short> idx(ibLen);
  makeSphere(0.005, 20, 10, vtx.begin(), idx.begin());
  g_spark.reset(new GeometryPNTBX(&vtx[0], &idx[0], vtx.size(), idx.size()));
}

static void drawStuff() {

  // Declare an empty uniforms
  Uniforms uniforms;

  // build & send proj. matrix to be stored by uniforms, 
  // as opposed to the current vtx shader
  
  const Matrix4 projmat = Matrix4::makeProjection(
    g_frustFovY, g_windowWidth / static_cast <double> (g_windowHeight),
    g_frustNear, g_frustFar);
  
  sendProjectionMatrix(uniforms, projmat);

  // use the current eye as the eyeRbt
  g_eye = g_skyNode;
  const RigTForm eyeRbt = getPathAccumRbt(g_world,g_eye);
  const RigTForm invEyeRbt = inv(eyeRbt);

  Cvec3 light = getPathAccumRbt(g_world, g_lightNode).getTranslation();
    const Cvec3 eyeLight = Cvec3(invEyeRbt * Cvec4(light,1)); 
    uniforms.put("uLight", eyeLight);
    Drawer drawer(invEyeRbt, uniforms);
    g_world->accept(drawer);
   
  // draw spheres
  // ==========
  // draw arcball wireframe, if it's active
  glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
  Matrix4 MVM, NMVM;
 
  MVM = rigTFormToMatrix(invEyeRbt * RigTForm());
  NMVM = normalMatrix(MVM);
    sendModelViewNormalMatrix(uniforms, MVM, NMVM);
    g_arcballMat ->draw(*g_sphere, uniforms);
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

  uniforms.put("uWidth",g_windowWidth);
  uniforms.put("uDist",-1);
  /*
  RigTForm fireworkRbt = g_fireworkRbt[0];
  for (list<Particle>::iterator it=g_firework1Particles.begin(), end=g_firework1Particles.end(); it != end; it++)
  {
    Matrix4 MVM = rigTFormToMatrix(invEyeRbt * (fireworkRbt * RigTForm((*it).getPosition())));
    Matrix4 NMVM = normalMatrix(MVM);
    sendModelViewNormalMatrix(uniforms, MVM, NMVM);
    g_sparkMat -> draw(*g_spark, uniforms);
  }

  fireworkRbt = g_fireworkRbt[1];
  for (list<Particle>::iterator it=g_firework2Particles.begin(), end=g_firework2Particles.end(); it != end; it++)
  {
    Matrix4 MVM = rigTFormToMatrix(invEyeRbt * (fireworkRbt * RigTForm((*it).getPosition())));
    Matrix4 NMVM = normalMatrix(MVM);
    sendModelViewNormalMatrix(uniforms, MVM, NMVM);
    g_sparkMat -> draw(*g_spark, uniforms);
  }

  fireworkRbt = g_fireworkRbt[2];
  for (list<Particle>::iterator it=g_firework3Particles.begin(), end=g_firework3Particles.end(); it != end; it++)
  {
    Matrix4 MVM = rigTFormToMatrix(invEyeRbt * (fireworkRbt * RigTForm((*it).getPosition())));
    Matrix4 NMVM = normalMatrix(MVM);
    sendModelViewNormalMatrix(uniforms, MVM, NMVM);
    g_sparkMat -> draw(*g_spark, uniforms);
  }

  fireworkRbt = g_fireworkRbt[3];
  for (list<Particle>::iterator it=g_firework4Particles.begin(), end=g_firework4Particles.end(); it != end; it++)
  {
    Matrix4 MVM = rigTFormToMatrix(invEyeRbt * (fireworkRbt * RigTForm((*it).getPosition())));
    Matrix4 NMVM = normalMatrix(MVM);
    sendModelViewNormalMatrix(uniforms, MVM, NMVM);
    g_sparkMat -> draw(*g_spark, uniforms);
  }
  
  fireworkRbt = g_fireworkRbt[4];
  for (list<Particle>::iterator it=g_firework5Particles.begin(), end=g_firework5Particles.end(); it != end; it++)
  {
    Matrix4 MVM = rigTFormToMatrix(invEyeRbt * (fireworkRbt * RigTForm((*it).getPosition())));
    Matrix4 NMVM = normalMatrix(MVM);
    sendModelViewNormalMatrix(uniforms, MVM, NMVM);
    g_sparkMat -> draw(*g_spark, uniforms);
  }
  */
}

static void display() {
  // No more glUseProgram
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  drawStuff(); // no more curSS

  glutSwapBuffers();

  checkGlErrors();
}

static void reshape(const int w, const int h) {
  g_windowWidth = w;
  g_windowHeight = h;
  glViewport(0, 0, w, h);
  std::cerr << "Size of window is now " << w << "x" << h << std::endl;
  glutPostRedisplay();
}

static void initFirework(Mesh* fireworkMesh) { //, list<Particle>* fireworkParticles) {
  int numVertices = (*fireworkMesh).getNumVertices();
  
  for (int i = 0 ; i < numVertices; i++)
  {
    Mesh::Vertex v = fireworkMesh->getVertex(i);
    g_fireworkParticles.push_back(Particle(v.getPosition()*0.2,v.getNormal()*0.05,Cvec3(1,0,0),rand() % 25 + 30));
    cout << "particle added" << endl;
    g_fireworkParticles.push_back(Particle(v.getPosition()*0.1,v.getNormal()*0.05,Cvec3(1,0,0),rand() % 25 + 30));
    g_fireworkParticles.push_back(Particle(v.getPosition()*0.05,v.getNormal()*0.05,Cvec3(1,0,0),rand() % 25 + 30));
  }

  return;
}

static void animateFireworks(int ms) {
 
  //g_f1active = true;
  // set off firework1
  float t = (float) ms / (float) g_msBetweenKeyFrames;
  
  bool dead = g_fireworkParticles.empty();

  if (!dead) {
    //adjust ALL the particles
    for (list<Particle>::iterator it = g_fireworkParticles.begin(), 
	   end = g_fireworkParticles.end(); it != end; it++) {
      it->update();
      if (it == g_fireworkParticles.begin()) { cout << "update" << endl; }
      if (it->dead() ) {
	list<Particle>::iterator it2 = it;
	it2++;
	g_fireworkParticles.erase(it);
	it = it2;
      }

      g_fireworks->reset(&g_fireworkParticles.front(), g_fireworkParticles.size());
    }
    
    glutPostRedisplay();

    glutTimerFunc(1000/g_animateFramesPerSecond,
		  animateFireworks,
		  ms+1000/g_animateFramesPerSecond);
  }
    
  // after done
  //g_f1active = false;
  //return;
}

static void animateFirework1(int ms) {
 
  g_f1active = true;
  // set off firework1
  float t = (float) ms / (float) g_msBetweenKeyFrames;
  
  bool dead = g_firework1Particles.empty();

  if (!dead) {
    //adjust all the particles
    for (list<Particle>::iterator it = g_firework1Particles.begin(), 
	   end = g_firework1Particles.end(); it != end; it++) {
      (*it).update();
      if ((*it).dead() ) {
	list<Particle>::iterator it2 = it;
	it2++;
	g_firework1Particles.erase(it);
	it = it2;
      }
    }
    
    glutTimerFunc(1000/g_animateFramesPerSecond,
		  animateFirework1,
		  ms+1000/g_animateFramesPerSecond);
  }
    
  // after done
  g_f1active = false;
  return;
}

static void animateFirework2(int ms) {
  g_f2active = true;
  // set off firework2
  float t = (float) ms / (float) g_msBetweenKeyFrames;
  
  bool dead = g_firework2Particles.empty();

  if (!dead) {
    //adjust all the particles
    for (list<Particle>::iterator it = g_firework2Particles.begin(), 
	   end = g_firework2Particles.end(); it != end; it++) {
      (*it).update();
      if ((*it).dead() ) {
	list<Particle>::iterator it2 = it;
	it2++;
	g_firework2Particles.erase(it);
	it = it2;
      }
    }
    
    glutTimerFunc(1000/g_animateFramesPerSecond,
		  animateFirework2,
		  ms+1000/g_animateFramesPerSecond);
  }
    
  // after done
  g_f2active = false;
  return;
}

static void animateFirework3(int ms) {
  g_f1active = true;
  // set off firework3
  float t = (float) ms / (float) g_msBetweenKeyFrames;
  
  bool dead = g_firework3Particles.empty();

  if (!dead) {
    //adjust all the particles
    for (list<Particle>::iterator it = g_firework3Particles.begin(), 
	   end = g_firework3Particles.end(); it != end; it++) {
      (*it).update();
      if ((*it).dead() ) {
	list<Particle>::iterator it2 = it;
	it2++;
	g_firework3Particles.erase(it);
	it = it2;
      }
    }
    
    glutTimerFunc(1000/g_animateFramesPerSecond,
		  animateFirework3,
		  ms+1000/g_animateFramesPerSecond);
  }
  
  // after done
  g_f3active = false;
  return;
}

static void animateFirework4(int ms) {
  g_f4active = true;
  // set off firework1
  float t = (float) ms / (float) g_msBetweenKeyFrames;
  
  bool dead = g_firework4Particles.empty();

  if (!dead) {
    //adjust all the particles
    for (list<Particle>::iterator it = g_firework4Particles.begin(), 
	   end = g_firework4Particles.end(); it != end; it++) {
      (*it).update();
      // cout << "update" << endl;
      if ((*it).dead() ) {
	list<Particle>::iterator it2 = it;
	it2++;
	g_firework4Particles.erase(it);
	it = it2;
      }
    }
    
    glutTimerFunc(1000/g_animateFramesPerSecond,
		  animateFirework4,
		  ms+1000/g_animateFramesPerSecond);
      
    // after done
    g_f4active = false;
    return;
  }
}

static void animateFirework5(int ms) {
  g_f5active = true;
  // set off firework1
  float t = (float) ms / (float) g_msBetweenKeyFrames;
  
  bool dead = g_firework5Particles.empty();
  
  if (!dead) {
    //adjust all the particles
    for (list<Particle>::iterator it = g_firework5Particles.begin(), 
	   end = g_firework5Particles.end(); it != end; it++) {
      (*it).update();
      if ((*it).dead() ) {
	list<Particle>::iterator it2 = it;
	it2++;
	g_firework5Particles.erase(it);
	it = it2;
      }
    }
    
    glutTimerFunc(1000/g_animateFramesPerSecond,
		  animateFirework5,
		  ms+1000/g_animateFramesPerSecond);
  }
  
  // after done
  g_f5active = false;
  return;
}


/*static void animateTimerCallback (int ms) {
  glutPostRedisplay();
  glutTimerFunc(1000/g_animateFramesPerSecond,
		animateTimerCallback, ms+1000/g_animateFramesPerSecond);
		}*/

static void updateFireworks(int x, int y) {

  float xcoord = (float) x / g_windowWidth * 2.22 - 1.11;//(((float) x)/ g_windowWidth);
  float ycoord = (float) y / g_windowHeight * 2.22 - 1.11;//(((float) y)/ g_windowHeight);

  cout << xcoord << ", " << ycoord << endl;
  
  if (g_currentFirework == 1) {
    float z = (g_fireworkRbt[0].getTranslation())[2];
    g_fireworkRbt[0] = RigTForm(Cvec3(xcoord, ycoord, z));
    initFirework(&g_firework1Mesh);//, &g_firework1Particles);
    //animateFirework1(0);
  }
  else if (g_currentFirework == 2) {
    float z = (g_fireworkRbt[1].getTranslation())[2];
    g_fireworkRbt[1] = RigTForm(Cvec3(xcoord, ycoord, z));
    initFirework(&g_firework2Mesh);//, &g_firework2Particles);
    //animateFirework2(0);
  }
  else if (g_currentFirework == 3) {
    float z = (g_fireworkRbt[2].getTranslation())[2];
    g_fireworkRbt[2] = RigTForm(Cvec3(xcoord, ycoord, z));
    initFirework(&g_firework3Mesh);//, &g_firework3Particles);
    //animateFirework3(0);
  }
  else if (g_currentFirework == 4) {
    float z = (g_fireworkRbt[3].getTranslation())[2];
    g_fireworkRbt[3] = RigTForm(Cvec3(xcoord, ycoord, z));
    initFirework(&g_firework4Mesh);//, &g_firework4Particles);
    //animateFirework4(0);
  }
  else if (g_currentFirework == 5) {
    //float z = (g_fireworkRbt[4].getTranslation())[2];
    float z = 0;
    //g_fireworkRbt[4] = RigTForm(Cvec3(xcoord, ycoord, z));
    initFirework(&g_firework5Mesh);//, &g_firework5Particles);
    //animateFirework5(0);
  }
}


static void mouse(const int button, const int state, const int x, const int y) {
  g_mouseClickX = x;
  g_mouseClickY = g_windowHeight - y - 1;  // conversion from GLUT window-coordinate-system to OpenGL window-coordinate-system

  g_mouseLClickButton |= (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN);
  g_mouseRClickButton |= (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN);
  g_mouseMClickButton |= (button == GLUT_MIDDLE_BUTTON && state == GLUT_DOWN);

  g_mouseLClickButton &= !(button == GLUT_LEFT_BUTTON && state == GLUT_UP);
  g_mouseRClickButton &= !(button == GLUT_RIGHT_BUTTON && state == GLUT_UP);
  g_mouseMClickButton &= !(button == GLUT_MIDDLE_BUTTON && state == GLUT_UP);

  g_mouseClickDown = g_mouseLClickButton || g_mouseRClickButton || g_mouseMClickButton;

  if (g_mouseClickDown) {
    updateFireworks(g_mouseClickX, g_mouseClickY);
  }
}

static vector<RigTForm> getFrame() {
  vector<RigTForm> thisFrameState;
  for (vector<shared_ptr<SgRbtNode> >::iterator iter = g_nodes.begin(), end = g_nodes.end(); iter != end; iter++)
  {
    thisFrameState.push_back((*iter)->getRbt());
  }
  return thisFrameState;
}


static void setFrame(vector<RigTForm> frame) {
  int i = 0;
  for (vector<shared_ptr<SgRbtNode> >::iterator iter = g_nodes.begin(), end = g_nodes.end(); 
       iter != end; iter++, i++)
    (*iter)->setRbt(frame[i]);
}

// animation
static Cvec3 lerp(Cvec3 c0, Cvec3 c1, double alpha) {
  c0 *= (1-alpha);
  c1 *= alpha;
  return (c0 + c1);
}

static Quat slerp(Quat q0, Quat q1, double alpha) {
  Quat q = pow(cn(q1 * inv(q0)), alpha) * q0;
  return q;
}

static Cvec3 vecSpline(Cvec3 c_1, Cvec3 c0, Cvec3 c1, Cvec3 c2, double alpha) {
  Cvec3 d0 = (c1 - c_1) / 6 + c0;
  Cvec3 e0 = (c0 - c2) / 6 + c1;

  Cvec3 f = lerp(c0, d0, alpha);
  Cvec3 g = lerp(d0, e0, alpha);
  Cvec3 h = lerp(e0, c1, alpha);
  
  Cvec3 m = lerp(f, g, alpha);
  Cvec3 n = lerp(g, h, alpha);

  return lerp(m, n, alpha);
}

static Quat quatSpline(Quat q_1, Quat q0, Quat q1, Quat q2, double alpha) {
  Quat d0 = pow(cn(q0 * inv(q_1)), (1/6)) * q0;
  Quat e0 = pow(cn(q2 * inv(q0)), (-1/6)) * q1;
  
  Quat f = slerp(q0, d0, alpha);
  Quat g = slerp(d0, e0, alpha);
  Quat h = slerp(e0, q1, alpha);

  Quat m = slerp(f, g, alpha);
  Quat n = slerp(g, h, alpha);

  return slerp(m, n, alpha);
}


bool interpolateAndDisplay(float t) {
  if(floor(t) + 3 == g_script.size())
  {
    g_playing = false;
    return true;
  }
  else
  {
    int floor_t = floor(t);
    vector<RigTForm> frame;
    double alpha = t - floor_t;

    if (floor_t + 1 > g_next.frameNumber) {
      g_minus1.increment();
      g_previous.increment();
      g_next.increment();
      g_plus1.increment();
    }

    for(int i = 0, end = (*(g_previous.frameIter)).size(); i < end; i++) {
      Cvec3 trans = vecSpline((*g_minus1.frameIter)[i].getTranslation(),
			      (*g_previous.frameIter)[i].getTranslation(),
			      (*g_next.frameIter)[i].getTranslation(),
			      (*g_plus1.frameIter)[i].getTranslation(),
			      alpha);

      Quat rot = quatSpline((*g_minus1.frameIter)[i].getRotation(),
			    (*g_previous.frameIter)[i].getRotation(),
			    (*g_next.frameIter)[i].getRotation(),
			    (*g_plus1.frameIter)[i].getRotation(),
			    alpha);

      RigTForm rbt = RigTForm(trans, rot);
      frame.push_back(rbt);
    }
    setFrame(frame);
    glutPostRedisplay();
    return false;
  }
}


static void keyboard(const unsigned char key, const int x, const int y) {
  switch (key) {
  case 27:
    exit(0);                                  // ESC
  case 'h':
    cout << " ============== H E L P ==============\n\n"
    << "h\t\thelp menu\n"
    << "s\t\tsave screenshot\n"
    << "f\t\tToggle flat shading on/off.\n"
    << "u\t\tUpdate current frame\n"
    << ">\t\tProceed to next frame\n"
    << "<\t\tReturn to previous frame\n"
    << "d\t\tDelete current frame\n"
    << "y\t\tPlay/stop animation\n"
    << "1\t\tFirework #1\n"
    << "2\t\tFirework #2\n"
    << "3\t\tFirework #3\n"
    << "4\t\tFirework #4\n"
    << "5\t\tFirework #5\n"
    << "drag left mouse to rotate\n"
    << "drag right mouse to translate\n" << endl;
    break;
  case 's':
    glFlush();
    writePpmScreenshot(g_windowWidth, g_windowHeight, "out.ppm");
    break;
    
  case 'u':
    if(g_currFrame == g_script.end())
      keyboard('n',g_mouseClickX,g_mouseClickY);
    else
      *g_currFrame = getFrame();
      break;
  case '>':
    if(g_currFrame == g_script.end())
      cout << "End of key frames.\n";
    else
    {
      list<vector<RigTForm> >::iterator temp = g_script.end();
      temp--;
      if (temp == g_currFrame)
      {
        cout << "End of key frames.\n";
	break;
      }
      g_currFrame++;
      setFrame(*g_currFrame);
    }
    break;
  case '<':
    if(g_currFrame == g_script.begin())
      cout << "Beginning of key frames.\n";
    else
    {
      if (g_currFrame == g_script.end())
	g_currFrame--;
      g_currFrame--;
      setFrame(*g_currFrame);
    }
    break;
  case 'd':
    if(g_currFrame != g_script.end())
    {
      list<vector<RigTForm> >::iterator temp = g_currFrame;
      if (temp == g_script.begin())
	temp++;
      else
	temp--;
      g_script.erase(g_currFrame);
      g_currFrame = temp;
      if (g_currFrame != g_script.end())
	setFrame(*g_currFrame);
    }
    break;
  case 'n':
    g_script.insert(g_currFrame,getFrame());
    break;

  // play animation
    
  case 'y':
    if(g_playing)
      g_playing = false;
    else
    {
      if(g_script.size() < 4)
      {
	cout << "Not enough frames to interpolate!\n";
	break;
      }
      g_playing = true;
      g_currFrame = g_script.begin();
      g_minus1 = frameIndex(-1);
      g_previous = frameIndex(0);
      g_next = frameIndex(1);
      g_plus1 = frameIndex(2);
      //animateTimerCallback(0);
    }
    break; 
  case '1':
    g_currentFirework = 1;
    cout << "Firework #1" << endl;
    break;
  case '2':
    g_currentFirework = 2;
    cout << "Firework #2" << endl;
    break;
  case '3':
    g_currentFirework = 3;
    cout << "Firework #3" << endl;
    break;
  case '4':
    g_currentFirework = 4;
    cout << "Firework #4" << endl;
    break;
  case '5':
    g_currentFirework = 5;
    cout << "Firework #5" << endl;
    break;
  }
  glutPostRedisplay();
}

static void initGlutState(int argc, char * argv[]) {
  glutInit(&argc, argv);                                  // initialize Glut based on cmd-line args
  glutInitDisplayMode(GLUT_RGBA|GLUT_DOUBLE|GLUT_DEPTH);  //  RGBA pixel channels and double buffering
  glutInitWindowSize(g_windowWidth, g_windowHeight);      // create a window
  glutCreateWindow("Final Project");                       // title the window

  glutDisplayFunc(display);                               // display rendering callback
  glutReshapeFunc(reshape);                               // window reshape callback
  glutMouseFunc(mouse);
  glutKeyboardFunc(keyboard);
}

static void initGLState() {
  glClearColor(0., 0., 0., 0.);
  glClearDepth(0.);
  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  glPixelStorei(GL_PACK_ALIGNMENT, 1);
  glCullFace(GL_BACK);
  glEnable(GL_CULL_FACE);
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_GREATER);
  glReadBuffer(GL_BACK);
  if (!g_Gl2Compatible)
    glEnable(GL_FRAMEBUFFER_SRGB);
}

static void initMaterials() {
  // Create some prototype materials
  Material diffuse("./shaders/basic-gl3.vshader", "./shaders/diffuse-gl3.fshader");
  Material solid("./shaders/basic-gl3.vshader", "./shaders/solid-gl3.fshader");
  Material specular("./shaders/basic-gl3.vshader", "./shaders/specular-gl3.fshader");
  //Material particle("./shaders/particle-gl3.vshader", "./shaders/particle-gl3.fshader");

  // copy diffuse prototype and set blue color
  g_greenDiffuseMat.reset(new Material(diffuse));
  g_greenDiffuseMat->getUniforms().put("uColor", Cvec3f(0.0, 0.4, 0.086));

  g_fireworkMat.reset(new Material(solid));
  g_fireworkMat->getUniforms().put("uColor", Cvec3f(0.384, 0.667, 0.165));
  //g_fireworkMat->getUniforms().put("uWidth",g_windowWidth);
  
  // copy solid prototype, and set to wireframed rendering
  g_arcballMat.reset(new Material(solid));
  g_arcballMat->getUniforms().put("uColor", Cvec3f(0.27f, 0.82f, 0.35f));
  g_arcballMat->getRenderStates().polygonMode(GL_FRONT_AND_BACK, GL_LINE);

  // copy solid prototype, and set to color white
  g_lightMat.reset(new Material(solid));
  g_lightMat->getUniforms().put("uColor", Cvec3f(1, 1, 1));

  // copy solid prototype, and set to color red
  g_sparkMat.reset(new Material(solid));
  g_sparkMat->getUniforms().put("uColor", Cvec3f(1, 0, 0));

  // pick shader
  g_pickingMat.reset(new Material("./shaders/basic-gl3.vshader", "./shaders/pick-gl3.fshader"));
};

static void initGeometry() {
  initGround();
  initCubes();
  initSpheres();
  initLights();
  initSparks();
  initMesh();
}

static void initScene() {
  g_world.reset(new SgRootNode());

  g_skyNode.reset(new SgRbtNode(RigTForm(Cvec3(0.0, 0.0, 3.0))));

  g_groundNode.reset(new SgRbtNode());
  g_groundNode->addChild(shared_ptr<SgGeometryShapeNode>(
							 new SgGeometryShapeNode(g_ground, g_greenDiffuseMat, Cvec3(0, g_groundY, 0))));

  g_fireworksNode.reset(new SgRbtNode());
  g_fireworksNode->addChild(shared_ptr<SgGeometryShapeNode>(new SgGeometryShapeNode(g_fireworks, g_fireworkMat, Cvec3(1, 0, 0)))); 
  g_world->addChild(g_lightNode);

  // defining subDiv node
  g_world->addChild(g_skyNode);
  g_world->addChild(g_groundNode);
  
  // add light
  g_lightNode.reset(new SgRbtNode(RigTForm(Cvec3(0, 1, 1))));

  g_world->addChild(g_lightNode);
  g_lightNode->addChild(shared_ptr<SgGeometryShapeNode>(new SgGeometryShapeNode(g_light, g_lightMat, Cvec3(0, 0, 0))));
}

int main(int argc, char * argv[]) {
  try {
    initGlutState(argc,argv);

    glewInit(); // load the OpenGL extensions

    cout << (g_Gl2Compatible ? "Will use OpenGL 2.x / GLSL 1.0" : "Will use OpenGL 3.x / GLSL 1.3") << endl;
    if ((!g_Gl2Compatible) && !GLEW_VERSION_3_0)
      throw runtime_error("Error: card/driver does not support OpenGL Shading Language v1.3");
    else if (g_Gl2Compatible && !GLEW_VERSION_2_0)
      throw runtime_error("Error: card/driver does not support OpenGL Shading Language v1.0");

    initGLState();
    initMaterials();
    initGeometry();
    initScene();
    g_currentPickedRbtNode = g_skyNode;
    dumpSgRbtNodes(g_world,g_nodes);
    animateFireworks(0);
    glutMainLoop();
    return 0;
  }
  catch (const runtime_error& e) {
    cout << "Exception caught: " << e.what() << endl;
    return -1;
  }
}
