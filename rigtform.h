#ifndef RIGTFORM_H
#define RIGTFORM_H

#include <iostream>
#include <cassert>

#include "matrix4.h"
#include "quat.h"

class RigTForm {
  Cvec3 t_; // translation component
  Quat r_;  // rotation component represented as a quaternion

public:
  RigTForm() : t_(0) {
    assert(norm2(Quat(1,0,0,0) - r_) < CS175_EPS2);
  }

  RigTForm(const Cvec3& t, const Quat& r) {
    //TODO
    t_ = t;
    r_ = r;
  }

  RigTForm(const Cvec3& t) {
    //TODO
    t_ = t;
    r_ = Quat();
  }

  RigTForm(const Quat& r) {
    //TODO
    t_ = Cvec3();
    r_ = r;
  }

  Cvec3 getTranslation() const {
    return t_;
  }

  Quat getRotation() const {
    return r_;
  }

  RigTForm& setTranslation(const Cvec3& t) {
    t_ = t;
    return *this;
  }

  RigTForm& setRotation(const Quat& r) {
    r_ = r;
    return *this;
  }

  Cvec4 operator * (const Cvec4& a) const {
    // TODO
    return (r_ * a) + Cvec4(t_, 0);
  }

  RigTForm operator * (const RigTForm& a) const {
    // TODO
    RigTForm n;
    n.setRotation(r_ * a.getRotation());
    n.setTranslation(t_ + Cvec3((r_ * Cvec4(a.getTranslation(), 0))));
    return n;
  }
};

inline RigTForm inv(const RigTForm& tform) {
  // TODO
  RigTForm r;
  Quat q = inv(tform.getRotation());
  Cvec4 t = inv(tform.getRotation()) * -Cvec4(tform.getTranslation(),0);
  r.setRotation(q);
  r.setTranslation(Cvec3(t));
  return r;
}

inline RigTForm transFact(const RigTForm& tform) {
  return RigTForm(tform.getTranslation());
}

inline RigTForm linFact(const RigTForm& tform) {
  return RigTForm(tform.getRotation());
}

inline Matrix4 rigTFormToMatrix(const RigTForm& tform) {
  // TODO
  Matrix4 m;
  
  // get translation factor
  m = Matrix4::makeTranslation(tform.getTranslation());

  // get and multiply by rotation factor
  m *= quatToMatrix(tform.getRotation());
  return m;
}

#endif
