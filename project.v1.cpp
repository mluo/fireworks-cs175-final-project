////////////////////////////////////////////////////////////////////////
//
//   Harvard University
//   CS175 : Computer Graphics
//   Professor Steven Gortler
//
////////////////////////////////////////////////////////////////////////

#include <string>
#include <vector>
#include <list>
#include <memory>
#include <stdexcept>
#include <iostream>
#include <fstream>
#if __GNUG__
#   include <tr1/memory>
#endif

#include <GL/glew.h>
#ifdef __MAC__
#   include <GLUT/glut.h>
#else
#   include <GL/glut.h>
#endif

#include "arcball.h"
#include "asstcommon.h"
#include "cvec.h"
#include "drawer.h"
#include "geometry.h"
#include "glsupport.h"
#include "matrix4.h"
#include "particle.h"
#include "picker.h"
#include "ppm.h"
#include "quat.h"
#include "rigtform.h"
#include "scenegraph.h"
#include "sgutils.h"
#include "geometrymaker.h"
#include "material.h"
#include "renderstates.h"
#include "texture.h"
#include "uniforms.h"
#include "mesh.h"


using namespace std;      // for string, vector, iostream, and other standard C++ stuff
using namespace std::tr1; // for shared_ptr

// G L O B A L S ///////////////////////////////////////////////////

// --------- IMPORTANT --------------------------------------------------------
// Before you start working on this assignment, set the following variable
// properly to indicate whether you want to use OpenGL 2.x with GLSL 1.0 or
// OpenGL 3.x+ with GLSL 1.3.
//
// Set g_Gl2Compatible = true to use GLSL 1.0 and g_Gl2Compatible = false to
// use GLSL 1.3. Make sure that your machine supports the version of GLSL you
// are using. In particular, on Mac OS X currently there is no way of using
// OpenGL 3.x with GLSL 1.3 when GLUT is used.
//
// If g_Gl2Compatible=true, shaders with -gl2 suffix will be loaded.
// If g_Gl2Compatible=false, shaders with -gl3 suffix will be loaded.
// To complete the assignment you only need to edit the shader files that get
// loaded
// ----------------------------------------------------------------------------
const bool g_Gl2Compatible = true;

static const float g_frustFovY = 60.0;    // 60 degree field of view in y direction
static const float g_frustNear = -0.1;    // near plane
static const float g_frustFar = -50.0;    // far plane
static const float g_groundY = -2.0;      // y coordinate of the ground
static const float g_groundSize = 10.0;   // half the ground length

static int g_numFiles = 0;

static int g_windowWidth = 512;
static int g_windowHeight = 512;
static bool g_mouseClickDown = false;    // is the mouse button pressed
static bool g_mouseLClickButton, g_mouseRClickButton, g_mouseMClickButton;
static bool g_zMotion = false; // keeps track of whether to use z-motion (hack for Macs)
static int g_mouseClickX, g_mouseClickY; // coordinates for mouse click event
static int g_activeShader = 0;
static bool g_worldSky = true;


inline void print(const Matrix4& m) {
  for (int i = 0; i < 4; i++)
  {
    for (int j = 0; j < 4; j++)
      cout << m(i,j) << ", ";
    cout << "\n";
  }
}

static bool g_picking = false;

static shared_ptr<Material> g_redDiffuseMat,
                            g_blueDiffuseMat,
			    g_greenDiffuseMat,
                            g_bumpFloorMat,
                            g_arcballMat,
                            g_pickingMat,
			    g_lightMat,
			    g_fireworkMat,
                            g_sparkMat;

shared_ptr<Material> g_overridingMaterial;

// Vertex buffer and index buffer associated with the ground and cube geometry
/*****/
static shared_ptr<Geometry> g_ground, g_cube, g_sphere, g_light, g_spark;
static shared_ptr<NonIndexedDynamicGeometryPN> g_firework1, g_firework2;
/*****/
static Mesh g_m, g_firework1Mesh, g_firework2Mesh;
static int g_numSubdivisions = 0;


// --------- Scene

static const Cvec3 g_light1(2.0, 3.0, 14.0), g_light2(-2, -3.0, -5.0); 
static RigTForm g_skyRbt = RigTForm(Cvec3(0.0, 0.25, 4.0));

// RigTForm array to keep track of objects/eyes; last entry is sky
static RigTForm g_objectRbt[3] = 
  {RigTForm(Cvec3(-1,0,0)),
   RigTForm(Cvec3(1,0,0)),
   g_skyRbt};
//static Cvec3f g_objectColors[2] = {Cvec3f(0.42, 0.28, 0.84),Cvec3f(0.047, 0.353, 0.651)};

// objects
static shared_ptr<SgRootNode> g_world;
static shared_ptr<SgRbtNode> g_skyNode, g_groundNode, g_robot1Node, g_robot2Node;
static shared_ptr<SgRbtNode> g_currentPickedRbtNode;
static shared_ptr<SgRbtNode> g_light1Node, g_light2Node;
static shared_ptr<SgRbtNode> g_firework1Node, g_firework2Node;

// particle systems
static list<Particle> g_firework1Particles;
static list<Particle> g_firework2Particles;

// counters
static int g_currObject = 2; // keeps track of object to manipulate; default is sky
static int g_currEye = 2; //keeps track of which object is eye; default is sky
static int g_totalObjects = 3;
static const int g_skyIndex = g_totalObjects - 1; // defines the position of the sky in the object matrix array

// eye
static shared_ptr<SgRbtNode> g_eye;
static void setEye() 
{
  switch(g_currEye)
  {
    case 0:
      g_eye = g_robot1Node;
      break;
    case 1:
      g_eye = g_robot2Node;
      break;
    case 2:
      g_eye = g_skyNode;
      break;
  }
}

// frames
static list<vector<RigTForm> > g_script;
static list<vector<RigTForm> >::iterator g_currFrame = g_script.begin();
static vector<shared_ptr<SgRbtNode> > g_nodes;

// timing/animation
static int g_msBetweenKeyFrames = 2000;       // 2 seconds between keyframes
static int g_animateFramesPerSecond = 60;     // frames to render per second during animation playback
static int g_animateMeshFramesPerSecond = 60; // frames to render per second for mesh animation
static bool g_playing = false;                // keep track of whether animation is playing

struct frameIndex {
  int frameNumber;
  list<vector<RigTForm> >::iterator frameIter;

  frameIndex(int n) {
    frameNumber = n;
    frameIter = g_script.begin();

    for (int i = -1; i < n; i++)
      frameIter++;
  }

  frameIndex& increment() {
    this->frameNumber += 1;
    (this->frameIter)++;
    return *this;
  }
};

frameIndex g_minus1 = frameIndex(-1);
frameIndex g_previous = frameIndex(0);
frameIndex g_next = frameIndex(1);
frameIndex g_plus1 = frameIndex(2);

bool g_smoothShading = false;

///////////////// END OF G L O B A L S //////////////////////////////////////////////////


static void initGround() {
  int ibLen, vbLen;
  getPlaneVbIbLen(vbLen, ibLen);

  // Temporary storage for cube GeometryPNTBX
  vector<VertexPNTBX> vtx(vbLen);
  vector<unsigned short> idx(ibLen);

  makePlane(g_groundSize*2, vtx.begin(), idx.begin());
  g_ground.reset(new GeometryPNTBX(&vtx[0], &idx[0], vbLen, ibLen));
}

static void initCubes() {
  int ibLen, vbLen;
  getCubeVbIbLen(vbLen, ibLen);


  // Temporary storage for cube GeometryPNTBX
  vector<VertexPNTBX> vtx(vbLen);
  vector<unsigned short> idx(ibLen);

  makeCube(1, vtx.begin(), idx.begin());
  g_cube.reset(new GeometryPNTBX(&vtx[0], &idx[0], vbLen, ibLen));
}

static void dumpMesh(vector<VertexPN>* vs, Mesh* m, int numFaces) {
  for (int i = 0; i < numFaces; i++)
  {
    Mesh::Face face = (*m).getFace(i);

    bool quad = (face.getNumVertices() == 4);

    Cvec3 p1 = (face.getVertex(0).getPosition())*0.1;
    Cvec3 p2 = (face.getVertex(1).getPosition())*0.1;
    Cvec3 p3 = (face.getVertex(2).getPosition())*0.1;
    Cvec3 p4;
    if (quad)
      p4 = (face.getVertex(3).getPosition())*0.1;

    Cvec3 n1 = face.getNormal();
    Cvec3 n2 = n1;
    Cvec3 n3 = n1;
    Cvec3 n4 = n1;

    if (g_smoothShading)
    {
      Mesh::VertexIterator it1(face.getVertex(0).getIterator()), it1_0(it1);
      float nbrFaces = 0;
      n1 = Cvec3(0.,0.,0.);
      do
      {
	n1 = n1 + it1.getFace().getNormal();
	nbrFaces = nbrFaces + 1;
      } while (++it1 != it1_0);
      n1 = n1 / nbrFaces;
      n1.normalize();

      Mesh::VertexIterator it2(face.getVertex(1).getIterator()), it2_0(it2);
      nbrFaces = 0;
      n2 = Cvec3(0.,0.,0.);
      do
      {
	n2 = n2 + it2.getFace().getNormal();
	nbrFaces = nbrFaces + 1;
      } while (++it2 != it2_0);
      n2 = n2 / nbrFaces;
      n2.normalize();

      Mesh::VertexIterator it3(face.getVertex(2).getIterator()), it3_0(it3);
      nbrFaces = 0;
      n3 = Cvec3(0.,0.,0.);
      do
      {
	n3 = n3 + it3.getFace().getNormal();
	nbrFaces = nbrFaces + 1;
      } while (++it3 != it3_0);
      n3 = n3 / nbrFaces;
      n3.normalize();

      if (quad)
      {
	Mesh::VertexIterator it4(face.getVertex(3).getIterator()), it4_0(it4);
	nbrFaces = 0;
	n4 = Cvec3(0.,0.,0.);
	do
	{
	  n4 = n4 + it4.getFace().getNormal();
	nbrFaces = nbrFaces + 1;
	} while (++it4 != it4_0);
	n4 = n4 / nbrFaces;
	n4.normalize();
      }
    }

    VertexPN vpn1 = VertexPN(p1[0],p1[1],p1[2],n1[0],n1[1],n1[2]);
    VertexPN vpn2 = VertexPN(p2[0],p2[1],p2[2],n2[0],n2[1],n2[2]);
    VertexPN vpn3 = VertexPN(p3[0],p3[1],p3[2],n3[0],n3[1],n3[2]);
    VertexPN vpn4 = VertexPN(p4[0],p4[1],p4[2],n4[0],n4[1],n4[2]);

    face.getVertex(0).setNormal(n1);
    //cout << "normal in  dump" << face.getVertex(0).getNormal()[0] << ", " << face.getVertex(0).getNormal()[1] << ", " << face.getVertex(0).getNormal()[2] << endl;
    face.getVertex(1).setNormal(n2);
    face.getVertex(2).setNormal(n3);
    if (quad)
      face.getVertex(3).setNormal(n4);

    (*vs).push_back(vpn1);
    (*vs).push_back(vpn2);
    (*vs).push_back(vpn3);

    if (quad)
    {
      (*vs).push_back(vpn1);
      (*vs).push_back(vpn3);
      (*vs).push_back(vpn4);
    }
  }

  /*for (int i = 0, n = (*vs).size(); i < n; i++)
    {
      cout << i << ": p[" << (*vs)[i].p[0] << "," << (*vs)[i].p[1] << "," << (*vs)[i].p[2] << "]  n[" << (*vs)[i].n[0] << "," << (*vs)[i].n[1] << "," << (*vs)[i].n[2] << "] \n";
      }*/
}

static void initMesh() {
  g_m = Mesh();
  g_m.load("cube.mesh");

  g_firework1Mesh = g_m;

  for (int i=0; i<2; i++)
    g_firework1Mesh.subdivide();

  int numFaces = g_firework1Mesh.getNumFaces();
  vector<VertexPN> vs1;
  dumpMesh(&vs1, &g_firework1Mesh, numFaces);

  g_firework1.reset(new NonIndexedDynamicGeometryPN(vs1.size()));
  g_firework1->reset(&vs1[0], vs1.size());

  g_firework2Mesh = g_m;

  for (int i=0; i<4; i++)
    g_firework2Mesh.subdivide();

  numFaces = g_firework2Mesh.getNumFaces();
  vector<VertexPN> vs2;
  dumpMesh(&vs2, &g_firework2Mesh, numFaces);

  g_firework2.reset(new NonIndexedDynamicGeometryPN(vs2.size()));
  g_firework2->reset(&vs2[0], vs2.size());
}

static void initSpheres() {
  int ibLen, vbLen;
  getSphereVbIbLen(20, 10, vbLen, ibLen);

  // Temporary storage for sphere GeometryPNTBX
  vector<VertexPNTBX> vtx(vbLen);
  vector<unsigned short> idx(ibLen);
  makeSphere(0.5, 20, 10, vtx.begin(), idx.begin());
  g_sphere.reset(new GeometryPNTBX(&vtx[0], &idx[0], vtx.size(), idx.size()));
}

static void initLights() {
  int ibLen, vbLen;
  getSphereVbIbLen(20, 10, vbLen, ibLen);

  // Temporary storage for sphere GeometryPNTBX
  vector<VertexPNTBX> vtx(vbLen);
  vector<unsigned short> idx(ibLen);
  makeSphere(0.2, 20, 10, vtx.begin(), idx.begin());
  g_light.reset(new GeometryPNTBX(&vtx[0], &idx[0], vtx.size(), idx.size()));
}

static void initSparks() {
  int ibLen, vbLen;
  getSphereVbIbLen(20, 10, vbLen, ibLen);

  // Temporary storage for sphere GeometryPNTBX
  vector<VertexPNTBX> vtx(vbLen);
  vector<unsigned short> idx(ibLen);
  makeSphere(0.02, 20, 10, vtx.begin(), idx.begin());
  g_spark.reset(new GeometryPNTBX(&vtx[0], &idx[0], vtx.size(), idx.size()));
}

static void drawStuff(bool picking) {

  // Declare an empty uniforms
  Uniforms uniforms;


  // build & send proj. matrix to be stored by uniforms, 
  // as opposed to the current vtx shader
  
  const Matrix4 projmat = Matrix4::makeProjection(
    g_frustFovY, g_windowWidth / static_cast <double> (g_windowHeight),
    g_frustNear, g_frustFar);
  
  sendProjectionMatrix(uniforms, projmat);

  // use the current eye as the eyeRbt
  const RigTForm eyeRbt = getPathAccumRbt(g_world,g_eye);
  const RigTForm invEyeRbt = inv(eyeRbt);

  if (!picking) 
  {
    Cvec3 light1 = getPathAccumRbt(g_world, g_light1Node).getTranslation();

    Cvec3 light2 = getPathAccumRbt(g_world, g_light2Node).getTranslation();

    const Cvec3 eyeLight1 = Cvec3(invEyeRbt * Cvec4(light1,1)); 
    const Cvec3 eyeLight2 = Cvec3(invEyeRbt * Cvec4(light2,1)); 
    uniforms.put("uLight", eyeLight1);
    uniforms.put("uLight2", eyeLight2);
    Drawer drawer(invEyeRbt, uniforms);
    g_world->accept(drawer);
  }
  else 
  {
    Picker picker(invEyeRbt, uniforms);

    // set overiding material to our picking material
    g_overridingMaterial = g_pickingMat;
    g_world->accept(picker);
    
    // unset overriding material
    g_overridingMaterial.reset();

    glFlush();
    g_currentPickedRbtNode = picker.getRbtNodeAtXY(g_mouseClickX, g_mouseClickY);
    if (g_currentPickedRbtNode == NULL)
      g_currentPickedRbtNode = g_skyNode;
  }

  // draw spheres
  // ==========
  // draw arcball wireframe, if it's active
  glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
  if (g_eye == g_skyNode && g_currentPickedRbtNode == g_skyNode && g_worldSky)
  {
    Matrix4 MVM = rigTFormToMatrix(invEyeRbt * RigTForm());
    Matrix4 NMVM = normalMatrix(MVM);
    sendModelViewNormalMatrix(uniforms, MVM, NMVM);
    g_arcballMat ->draw(*g_sphere, uniforms);
  }
  
  if (g_eye != g_currentPickedRbtNode)
  {
    Matrix4 MVM = rigTFormToMatrix(invEyeRbt * getPathAccumRbt(g_world,g_currentPickedRbtNode));
    Matrix4 NMVM = normalMatrix(MVM);
    sendModelViewNormalMatrix(uniforms, MVM, NMVM);
    g_arcballMat -> draw(*g_sphere, uniforms);
  }
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  /*
  for (int i = 0, size = g_firework1Particles.size(); i<size; i++)
  {
    Matrix4 MVM = rigTFormToMatrix(invEyeRbt * getPathAccumRbt(g_world,g_firework1Node));
    Matrix4 NMVM = normalMatrix(MVM);
    sendModelViewNormalMatrix(uniforms, MVM, NMVM);
    g_redDiffuseMat -> draw(*g_sphere, uniforms);
    }*/

  RigTForm fireworkRbt = getPathAccumRbt(g_world,g_firework1Node);
  for (list<Particle>::iterator it=g_firework1Particles.begin(), end=g_firework1Particles.end(); it != end; it++)
  {
    Matrix4 MVM = rigTFormToMatrix(invEyeRbt * (fireworkRbt * RigTForm((*it).getPosition())));
    Matrix4 NMVM = normalMatrix(MVM);
    sendModelViewNormalMatrix(uniforms, MVM, NMVM);
    g_redDiffuseMat -> draw(*g_spark, uniforms);
  }

  fireworkRbt = getPathAccumRbt(g_world,g_firework2Node);
  for (list<Particle>::iterator it=g_firework2Particles.begin(), end=g_firework2Particles.end(); it != end; it++)
  {
    Matrix4 MVM = rigTFormToMatrix(invEyeRbt * (fireworkRbt * RigTForm((*it).getPosition())));
    Matrix4 NMVM = normalMatrix(MVM);
    sendModelViewNormalMatrix(uniforms, MVM, NMVM);
    g_redDiffuseMat -> draw(*g_spark, uniforms);
  }
}

static void display() {
  // No more glUseProgram
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  drawStuff(false); // no more curSS

  glutSwapBuffers();

  checkGlErrors();
}

static void pick() {
  // We need to set the clear color to black, for pick rendering.
  // so let's save the clear color
  GLdouble clearColor[4];
  glGetDoublev(GL_COLOR_CLEAR_VALUE, clearColor);

  glClearColor(0, 0, 0, 0);

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // No more glUseProgram
  drawStuff(true); // no more curSS

  // Uncomment below and comment out the glutPostRedisplay in mouse(...) call back
  // to see result of the pick rendering pass
  glutSwapBuffers();

  //Now set back the clear color
  glClearColor(clearColor[0], clearColor[1], clearColor[2], clearColor[3]);

  checkGlErrors();
}

static void reshape(const int w, const int h) {
  g_windowWidth = w;
  g_windowHeight = h;
  glViewport(0, 0, w, h);
  std::cerr << "Size of window is now " << w << "x" << h << std::endl;
  glutPostRedisplay();
}

static RigTForm makeMixedFrame(RigTForm X, RigTForm Y) {
  return transFact(X) * linFact(Y);
}

static RigTForm doMtoOwrtA(RigTForm M, RigTForm O, RigTForm A) {
  return (A * M * inv(A) * O);
}

// calculates z-coordinate corresponding to a click
static double findZ(double x, double cx, double y, double cy, double r) {
  // (x-cx)^2 + (y-cy)^2 + (z-0)^2 - r^2 = 0
  // where (x,y) is the coordinate of the click and (cx,cy) is the center
  double dx = x-cx;
  double dy = y-cy;

  // solve for z^2 first
  double z_squared = pow(r,2) - pow(dx,2) - pow(dy,2);

  // if z^2 >= 0, we're on the ball
  if (z_squared >= 0)
    return sqrt(z_squared);

  // otherwise, we're off the ball so clamp to nearest point
  else
    return 0.;
}

static void motion(const int x, const int y) {
  const double dx = x - g_mouseClickX;
  const double dy = g_windowHeight - y - 1 - g_mouseClickY;

  RigTForm m,a,l;
  int rot,trans;
  bool arcball = false;
  Cvec2 outCenter;
  double outRadius;

  RigTForm C_l = getPathAccumRbt(g_world,g_currentPickedRbtNode);
  RigTForm C_s = getPathAccumRbt(g_world,g_currentPickedRbtNode,1);
  RigTForm C_e = getPathAccumRbt(g_world,g_eye);
  
  /* check which combination of object and eye is used
   * multiply rotation and translation factors accordingly
   * construct auxillary matrix accordingly
   */
  // if we're dealing with a robot
  if (g_currentPickedRbtNode != g_skyNode)
  {
        // invert rotation if we're viewing from the object itself
  	rot = (g_currentPickedRbtNode == g_eye) ? -1 : 1;
  	trans = 1;

	// A = C(l)_t*C(e)_r
        RigTForm A = makeMixedFrame(C_l,C_e);

	// l = inv(C(s)) * C(l)
	l = inv(C_s) * C_l;

	// a = inv(C(s)) * A
	a = inv(C_s) * A;
	
	// if we're manipulating a cube from a view other than itself
	if (g_currentPickedRbtNode != g_eye)
	{
	  // construct projection matrix
	  Matrix4 projmat = Matrix4::makeProjection(
	    g_frustFovY, g_windowWidth / static_cast <double> (g_windowHeight),
	    g_frustNear, g_frustFar);
	  
	  // construct center in eye coordinates
	  Cvec3 center = Cvec3(
	    rigTFormToMatrix(inv(C_e) * C_l) * Cvec4(0,0,0,1));

	  // determine center and radius in screen pixels
	  bool gSSC = getScreenSpaceCircle(
            center, 1.0, projmat, g_frustNear, g_frustFovY, g_windowWidth, g_windowHeight, outCenter, outRadius);
	  
	  // acknowledge that we're in arcball mode
	  arcball = true;
	}

  }
  // if we're dealing with the sky camera
  else if (g_currentPickedRbtNode == g_skyNode && g_eye == g_skyNode)
  {
    // invert rotation
    rot = -1;
    
    // if we're manipulating with repsect to world-sky frame
    if (g_worldSky)
    {
      // invert translation
      trans = -1;

      // a = E_r
      RigTForm A = linFact(getPathAccumRbt(g_world,g_eye));

      // l = inv(C(s)) * C(l)
      l = inv(C_s) * C_l;
      
      // a = inv(C(s)) * A
      a = inv(C_s) * A;

      // construct projection matrix
      Matrix4 projmat = Matrix4::makeProjection(
	g_frustFovY, g_windowWidth / static_cast <double> (g_windowHeight),
	g_frustNear, g_frustFar);
		     
      // construct center in eye coordinates
      Cvec3 center = Cvec3(inv(getPathAccumRbt(g_world,g_eye)) * Cvec4(0,0,0,1));

      // determine center and radius in screen pixels
      bool gSSC = getScreenSpaceCircle(
        center, 1.0, projmat, g_frustNear, g_frustFovY, g_windowWidth, g_windowHeight, outCenter, outRadius);

      // acknowledge that we're in arcball mode
      arcball = true;
    }
    // if we're manipulating with respect to sky-sky frame
    else
    {
      // don't invert translation
      trans = 1;

      // A = E
      RigTForm A = getPathAccumRbt(g_world,g_eye);

      // l = inv(C(s)) * C(l)
      l = inv(C_s) * C_l;
      
      // a = inv(C(s)) * A
      a = inv(C_s) * A;
    }
  }
  
  if (g_mouseLClickButton && !g_mouseRClickButton) { // left button down?
    // check if in arcball mode
    if (arcball)
    {
        // using calculated center and radius in screen coordinates and position of click, determine z's 
	double z = findZ(g_mouseClickX, outCenter[0], g_mouseClickY, outCenter[1], outRadius);
        double z2 = findZ(x, outCenter[0], g_windowHeight - y - 1, outCenter[1], outRadius);

	// construct vectors from center of ball to clicked point
	Cvec3 v1 = Cvec3(g_mouseClickX,g_mouseClickY,z) - Cvec3(outCenter[0],outCenter[1],0);
	Cvec3 v2 = Cvec3(x,g_windowHeight - y - 1,z2) - Cvec3(outCenter[0],outCenter[1],0);

	// normalize vectors
	v1.normalize();
	v2.normalize();

	// construct composite quaternion for rotation
	Quat q1(0, v1);
	Quat q2(0, v2);
	Quat q = q2 * q1;
	m.setRotation(q);
	
	// invert quaternion if manipulating world-sky
	if (g_currentPickedRbtNode == g_skyNode)
	  m = inv(m);

    }
    // if not in arcball mode, just do a regular rotation
    else
    {
      Quat q = Quat::makeXRotation(-dy*rot) * Quat::makeYRotation(dx*rot);
      m.setRotation(q);
    }
  }
  else if (g_mouseRClickButton && (!g_mouseLClickButton && !g_zMotion)) { // right button down?
    Cvec3 t = Cvec3(dx, dy, 0) * 0.01 * trans;
    m.setTranslation(t);
  }
  else if (g_mouseMClickButton || 
	  (g_mouseLClickButton && g_mouseRClickButton) || 
	  (g_mouseRClickButton && g_zMotion)) {  // middle or (left and right) button down?
    Cvec3 t = Cvec3(0, 0, -dy) * 0.01 * trans;
    m.setTranslation(t);
  }
  
  // if mouse is down, apply transformation and redraw the frame
  if (g_mouseClickDown) {
    g_currentPickedRbtNode->setRbt(doMtoOwrtA(m,l,a));
    glutPostRedisplay(); // we always redraw if we changed the scene
  }

  g_mouseClickX = x;
  g_mouseClickY = g_windowHeight - y - 1;

}


static void mouse(const int button, const int state, const int x, const int y) {
  g_mouseClickX = x;
  g_mouseClickY = g_windowHeight - y - 1;  // conversion from GLUT window-coordinate-system to OpenGL window-coordinate-system

  if (g_picking)
  {
    pick();
    g_picking = false;
    glutPostRedisplay();
  }

  g_mouseLClickButton |= (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN);
  g_mouseRClickButton |= (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN);
  g_mouseMClickButton |= (button == GLUT_MIDDLE_BUTTON && state == GLUT_DOWN);

  g_mouseLClickButton &= !(button == GLUT_LEFT_BUTTON && state == GLUT_UP);
  g_mouseRClickButton &= !(button == GLUT_RIGHT_BUTTON && state == GLUT_UP);
  g_mouseMClickButton &= !(button == GLUT_MIDDLE_BUTTON && state == GLUT_UP);

  g_mouseClickDown = g_mouseLClickButton || g_mouseRClickButton || g_mouseMClickButton;
}

static vector<RigTForm> getFrame() {
  vector<RigTForm> thisFrameState;
  for (vector<shared_ptr<SgRbtNode> >::iterator iter = g_nodes.begin(), end = g_nodes.end(); iter != end; iter++)
  {
    thisFrameState.push_back((*iter)->getRbt());
  }
  return thisFrameState;
}


static void setFrame(vector<RigTForm> frame) {
  int i = 0;
  for (vector<shared_ptr<SgRbtNode> >::iterator iter = g_nodes.begin(), end = g_nodes.end(); 
       iter != end; iter++, i++)
    (*iter)->setRbt(frame[i]);
}

// animation
static Cvec3 lerp(Cvec3 c0, Cvec3 c1, double alpha) {
  c0 *= (1-alpha);
  c1 *= alpha;
  return (c0 + c1);
}

static Quat slerp(Quat q0, Quat q1, double alpha) {
  Quat q = pow(cn(q1 * inv(q0)), alpha) * q0;
  return q;
}

static Cvec3 vecSpline(Cvec3 c_1, Cvec3 c0, Cvec3 c1, Cvec3 c2, double alpha) {
  Cvec3 d0 = (c1 - c_1) / 6 + c0;
  Cvec3 e0 = (c0 - c2) / 6 + c1;

  Cvec3 f = lerp(c0, d0, alpha);
  Cvec3 g = lerp(d0, e0, alpha);
  Cvec3 h = lerp(e0, c1, alpha);
  
  Cvec3 m = lerp(f, g, alpha);
  Cvec3 n = lerp(g, h, alpha);

  return lerp(m, n, alpha);
}

static Quat quatSpline(Quat q_1, Quat q0, Quat q1, Quat q2, double alpha) {
  Quat d0 = pow(cn(q0 * inv(q_1)), (1/6)) * q0;
  Quat e0 = pow(cn(q2 * inv(q0)), (-1/6)) * q1;
  
  Quat f = slerp(q0, d0, alpha);
  Quat g = slerp(d0, e0, alpha);
  Quat h = slerp(e0, q1, alpha);

  Quat m = slerp(f, g, alpha);
  Quat n = slerp(g, h, alpha);

  return slerp(m, n, alpha);
}


bool interpolateAndDisplay(float t) {
  if(floor(t) + 3 == g_script.size())
  {
    g_playing = false;
    return true;
  }
  else
  {
    int floor_t = floor(t);
    vector<RigTForm> frame;
    double alpha = t - floor_t;

    if (floor_t + 1 > g_next.frameNumber) {
      g_minus1.increment();
      g_previous.increment();
      g_next.increment();
      g_plus1.increment();
    }

    for(int i = 0, end = (*(g_previous.frameIter)).size(); i < end; i++) {
      Cvec3 trans = vecSpline((*g_minus1.frameIter)[i].getTranslation(),
			      (*g_previous.frameIter)[i].getTranslation(),
			      (*g_next.frameIter)[i].getTranslation(),
			      (*g_plus1.frameIter)[i].getTranslation(),
			      alpha);

      Quat rot = quatSpline((*g_minus1.frameIter)[i].getRotation(),
			    (*g_previous.frameIter)[i].getRotation(),
			    (*g_next.frameIter)[i].getRotation(),
			    (*g_plus1.frameIter)[i].getRotation(),
			    alpha);

      RigTForm rbt = RigTForm(trans, rot);
      frame.push_back(rbt);
    }
    setFrame(frame);
    glutPostRedisplay();
    return false;
  }
}



static void animateTimerCallback(int ms) {
  if(!g_playing)
    return;

  float t = (float)ms/(float)g_msBetweenKeyFrames;
  
  bool endReached = interpolateAndDisplay(t);
  if(!endReached)
    glutTimerFunc(1000/g_animateFramesPerSecond,
		  animateTimerCallback,
		  ms+1000/g_animateFramesPerSecond);
  else
  {
    g_currFrame = g_script.begin();
    setFrame(*g_currFrame);
    glutPostRedisplay();
  }
}

static void initFirework1() {
  int numVertices = g_firework1Mesh.getNumVertices();

  for (int i = 0 ; i < numVertices; i++)
  {
    Mesh::Vertex v = g_firework1Mesh.getVertex(i);
    cout << "normal" << g_firework1Mesh.getVertex(i).getNormal()[0] << ", " << g_firework1Mesh.getVertex(i).getNormal()[1] << ", " << g_firework1Mesh.getVertex(i).getNormal()[2] << endl;
    g_firework1Particles.push_back(Particle(v.getPosition(),v.getNormal()*0.1,Cvec3(1,0,0)));
  }
}

static void initFirework2() {
  cout << "init2" << endl;

  int numVertices = g_firework2Mesh.getNumVertices();

  for (int i = 0 ; i < numVertices; i++)
  {
    Mesh::Vertex v = g_firework2Mesh.getVertex(i);
    g_firework2Particles.push_back(Particle(v.getPosition(),v.getNormal()*0.0001,Cvec3(1,0,0)));
  }
}

static void animateFirework1(int ms) {
  
  float t = (float) ms / (float) g_msBetweenKeyFrames;

  bool dead = g_firework1Particles.empty();

  if (!dead) {
    //adjust all the particles
    for (list<Particle>::iterator it = g_firework1Particles.begin(), end = g_firework1Particles.end(); 
	 it != end; it++) {
      if (it == g_firework1Particles.begin())
	cout << (*it).getPosition()[0] << ", " << (*it).getPosition()[1] << ", " << (*it).getPosition()[2] << endl; 
      (*it).update();
      if (it == g_firework1Particles.begin())
	cout << (*it).getPosition()[0] << ", " << (*it).getPosition()[1] << ", " << (*it).getPosition()[2] << endl;
      if ((*it).dead() ) {
	cout << "dead" << endl;
	list<Particle>::iterator it2 = it;
	it2++;
	g_firework1Particles.erase(it);
	it = it2;
      }
    }
    
    glutPostRedisplay();

    glutTimerFunc(1000/g_animateFramesPerSecond,
		  animateFirework1,
		  ms+1000/g_animateFramesPerSecond);
  }
  //else return;

  return;
}

static void animateFirework2(int ms) {

  cout << "animate2" << endl;
  
  float t = (float) ms / (float) g_msBetweenKeyFrames;

  bool dead = g_firework2Particles.empty();

  if (!dead) {
    //adjust all the particles
    for (list<Particle>::iterator it = g_firework2Particles.begin(), end = g_firework2Particles.end(); 
	 it != end; it++) {
      (*it).update();
      if ((*it).dead() ) {
	cout << "dead2" << endl;
	//return;
	list<Particle>::iterator it2 = it;
	it2++;
	g_firework2Particles.erase(it);
	it = it2;
      }
    }
    
    glutPostRedisplay();

    glutTimerFunc(1000/g_animateFramesPerSecond,
		  animateFirework1,
		  ms+1000/g_animateFramesPerSecond);
  }


  return;
  //else return;
}

/*
static void animateMeshCube(int ms) {

  Mesh meshCopy = g_m;

  for (int i=0, numVertices = meshCopy.getNumVertices(); i<numVertices; i++)
  {
    Mesh::Vertex vertex = meshCopy.getVertex(i);
    Cvec3 position = vertex.getPosition();
    float scale = abs(sin(i + ms*0.0012));
    position = position * scale;
    vertex.setPosition(position);
  }

  for (int i=0; i<g_numSubdivisions; i++)
    meshCopy.subdivide();

  int numFaces = meshCopy.getNumFaces();
  vector<VertexPN> vs;
  dumpMesh(&vs, meshCopy, numFaces);

  g_mesh->reset(&vs[0], vs.size());

  glutPostRedisplay();
  glutTimerFunc(1000/g_animateMeshFramesPerSecond,
		animateMeshCube,
		ms+1000/g_animateMeshFramesPerSecond);
		}*/

static void readFile() {
  ifstream inFile("incoming.txt");
  bool is_done = false;
  g_script.empty();
  //static list<vector<RigTForm> > thisScript;
  if (inFile.is_open()) {
    while (inFile.good()){
      // iterate until 22 rbts are created 
      // then they are stored in a frame
      vector<RigTForm> thisFrame;
      for (int numRbts = 0; numRbts < 22; numRbts++) {
	double nums[7];
	
	// iterate until 7 chars (each entry is 7 chars)
	for (int i = 0; i < 7; i++) {
	  string ch;
	  getline(inFile, ch, ',');
	  // check to see if 'f', which means end
	  
	  nums[i] = atof(ch.c_str());
	}
	Cvec3 trans = Cvec3(nums[0], nums[1], nums[2]);
	Quat rot = Quat(nums[3], nums[4], nums[5], nums[6]);
	RigTForm thisRbt = RigTForm(trans, rot);
	thisFrame.push_back(thisRbt);
	
      }
      // put frame into the script
      g_script.push_back(thisFrame);
    }
  }
  // pop off the last frame which is always garbage
  g_script.pop_back();
  // now set current to the begin
  g_currFrame = g_script.begin();
  inFile.close();
}


static void writeFile() {
  char out[11];
  sprintf(out, "outfile%03d.txt", g_numFiles);
  FILE *outFile = fopen(out, "w");
  
 if (outFile == NULL) {
    cout << "Unable to write to file." << endl;
    fclose(outFile);
    return;
  }
  
 list<vector<RigTForm> >::iterator iter = g_script.begin();
  
  for (int i = 0; i <g_script.size(); i++) {
    vector<RigTForm> thisFrame = (*iter);
    
    for (int i = 0; i < thisFrame.size(); i++) {
      RigTForm rbt = thisFrame[i];
      Cvec3 t = rbt.getTranslation();
      Quat q = rbt.getRotation();
      char line[100];
      sprintf(line, "%f, %f, %f, %f, %f, %f, %f,", t[0], t[1], t[2], q[0], q[1], q[2], q[3]);
      
      int len = strlen(line);
    
      fwrite(line, sizeof(char), len, outFile);	
    }
   
    iter++;
  }
  fclose(outFile);
}





static void keyboard(const unsigned char key, const int x, const int y) {
  switch (key) {
  case 27:
    exit(0);                                  // ESC
  case 'h':
    cout << " ============== H E L P ==============\n\n"
    << "h\t\thelp menu\n"
    << "s\t\tsave screenshot\n"
    << "f\t\tToggle flat shading on/off.\n"
    << "v\t\tCycle view\n"
    << "z\t\tSwitch to/from z-translation\n"
    << "p\t\tActivate picking mode\n"
    << "u\t\tUpdate current frame\n"
    << ">\t\tProceed to next frame\n"
    << "<\t\tReturn to previous frame\n"
    << "d\t\tDelete current frame\n"
    << "y\t\tPlay/stop animation\n"
    << "+\t\tIncrease animation speed\n"
    << "-\t\tDecrease animation speed\n"
    << "i\t\tRead in frames from file\n"
    << "w\t\tOutput frames to file\n"
    << "f\t\tToggle between smooth shading and flat shading\n"
    << "0\t\tIncrease subdivision steps\n"
    << "9\t\tDecrease subdivision steps\n"
    << "7\t\tDecrease mesh animation speed\n"
    << "8\t\tIncrease mesh animation speed\n"
    << "drag left mouse to rotate\n"
    << "drag right mouse to translate\n" << endl;
    break;
  case 's':
    glFlush();
    writePpmScreenshot(g_windowWidth, g_windowHeight, "out.ppm");
    break;
  case 'v':
    g_currEye = (g_currEye+1)%g_totalObjects;
    setEye();
    
    if (g_currentPickedRbtNode == g_skyNode && g_eye != g_skyNode)
    {
      g_currentPickedRbtNode = g_robot1Node;
      cout << "The current object is now robot 1\n";
    }
      
    if (g_currEye == 0)
      cout << "The current view is robot 1.\n";
    else if (g_currEye ==  1)
      cout << "The current view is robot 2.\n";
    else if (g_currEye ==  2)
      cout << "The current view is the sky camera frame.\n";

    break;
  case 'm':
    g_worldSky ^= 1;
    if (g_worldSky)
      cout << "Editing with respect to world-sky frame.\n";
    else
      cout << "Editing with respect to sky-sky frame.\n";
    break;
  case 'z':
    g_zMotion ^= 1;
    break;
  case 'p':
    cout << "Picking mode:\n";
    g_picking ^= 1;
    break;

  // key-frame manipulation SPACE
  case 32:
    // if not empty aka size not zero
    if (g_currFrame != g_script.end()){
      //copy g_currentFrame to scene graph
      setFrame(*g_currFrame);
    }
    break;
  case 'u':
    if(g_currFrame == g_script.end())
      keyboard('n',g_mouseClickX,g_mouseClickY);
    else
      *g_currFrame = getFrame();
      break;
  case '>':
    if(g_currFrame == g_script.end())
      cout << "End of key frames.\n";
    else
    {
      list<vector<RigTForm> >::iterator temp = g_script.end();
      temp--;
      if (temp == g_currFrame)
      {
        cout << "End of key frames.\n";
	break;
      }
      g_currFrame++;
      setFrame(*g_currFrame);
    }
    break;
  case '<':
    if(g_currFrame == g_script.begin())
      cout << "Beginning of key frames.\n";
    else
    {
      if (g_currFrame == g_script.end())
	g_currFrame--;
      g_currFrame--;
      setFrame(*g_currFrame);
    }
    break;
  case 'd':
    if(g_currFrame != g_script.end())
    {
      list<vector<RigTForm> >::iterator temp = g_currFrame;
      if (temp == g_script.begin())
	temp++;
      else
	temp--;
      g_script.erase(g_currFrame);
      g_currFrame = temp;
      if (g_currFrame != g_script.end())
	setFrame(*g_currFrame);
    }
    break;
  case 'n':
    g_script.insert(g_currFrame,getFrame());
    break;

  // play animation
  case 'y':
    if(g_playing)
      g_playing = false;
    else
    {
      if(g_script.size() < 4)
      {
	cout << "Not enough frames to interpolate!\n";
	break;
      }
      g_playing = true;
      g_currFrame = g_script.begin();
      g_minus1 = frameIndex(-1);
      g_previous = frameIndex(0);
      g_next = frameIndex(1);
      g_plus1 = frameIndex(2);
      animateTimerCallback(0);
    }
    break; 
  case '+':
    if (g_animateFramesPerSecond >= 10)
      g_animateFramesPerSecond -= 10;
    break;
  case '-':
    if (g_animateFramesPerSecond <= 150)
      g_animateFramesPerSecond += 10;
    break;
  
  
 case 'i':
   readFile();
   cout << "Successfully read file" << endl;
   break;
  

 case 'w':
   g_numFiles++;
   writeFile();
   cout << "Outputted key frames in file: outfile" << g_numFiles << endl;
   break;

 // mesh
 case 'f':
   g_smoothShading ^= 1;
   break;
 case '0':
   if (g_numSubdivisions < 6)
     g_numSubdivisions = g_numSubdivisions++;
   cout << "Subdivision levels = " << g_numSubdivisions << "\n";
   break;
 case'9':
   if (g_numSubdivisions > 0)
     g_numSubdivisions--; 
   cout << "Subdivision levels = " << g_numSubdivisions << "\n";
   break;
 case '7':
    if (g_animateMeshFramesPerSecond <= 850)
      g_animateMeshFramesPerSecond *= 2;
    break;
 case '8':
    if (g_animateMeshFramesPerSecond >= 10)
      g_animateMeshFramesPerSecond /= 2;
    break;

  }

  glutPostRedisplay();
}

static void initGlutState(int argc, char * argv[]) {
  glutInit(&argc, argv);                                  // initialize Glut based on cmd-line args
  glutInitDisplayMode(GLUT_RGBA|GLUT_DOUBLE|GLUT_DEPTH);  //  RGBA pixel channels and double buffering
  glutInitWindowSize(g_windowWidth, g_windowHeight);      // create a window
  glutCreateWindow("Assignment 7");                       // title the window

  glutDisplayFunc(display);                               // display rendering callback
  glutReshapeFunc(reshape);                               // window reshape callback
  glutMotionFunc(motion);                                 // mouse movement callback
  glutMouseFunc(mouse);                                   // mouse click callback
  glutKeyboardFunc(keyboard);
}

static void initGLState() {
  glClearColor(0., 0., 0., 0.);
  glClearDepth(0.);
  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  glPixelStorei(GL_PACK_ALIGNMENT, 1);
  glCullFace(GL_BACK);
  glEnable(GL_CULL_FACE);
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_GREATER);
  glReadBuffer(GL_BACK);
  if (!g_Gl2Compatible)
    glEnable(GL_FRAMEBUFFER_SRGB);
}

static void initMaterials() {
  // Create some prototype materials
  Material diffuse("./shaders/basic-gl3.vshader", "./shaders/diffuse-gl3.fshader");
  Material solid("./shaders/basic-gl3.vshader", "./shaders/solid-gl3.fshader");
  Material specular("./shaders/basic-gl3.vshader", "./shaders/specular-gl3.fshader");
  // Material normal("./shaders/normal-gl3.vshader", "./shaders/normal-gl3.fshader");

  // copy diffuse prototype and set red color
  g_redDiffuseMat.reset(new Material(diffuse));
  g_redDiffuseMat->getUniforms().put("uColor", Cvec3f(1, 0, 0));

  // copy diffuse prototype and set blue color
  g_blueDiffuseMat.reset(new Material(diffuse));
  g_blueDiffuseMat->getUniforms().put("uColor", Cvec3f(0, 0, 1));

  // copy diffuse prototype and set red color
  g_greenDiffuseMat.reset(new Material(diffuse));
  g_greenDiffuseMat->getUniforms().put("uColor", Cvec3f(0.0, 0.4, 0.086));

  g_fireworkMat.reset(new Material(specular));
  g_fireworkMat->getUniforms().put("uColor", Cvec3f(0.384, 0.667, 0.165));

  // normal mapping material
  /* g_bumpFloorMat.reset(new Material (normal));
  g_bumpFloorMat->getUniforms().put("uTexColor", shared_ptr<ImageTexture>(new ImageTexture("Fieldstone.ppm", true)));
  g_bumpFloorMat->getUniforms().put("uTexNormal", shared_ptr<ImageTexture>(new ImageTexture("FieldstoneNormal.ppm", false)));*/

  // copy solid prototype, and set to wireframed rendering
  g_arcballMat.reset(new Material(solid));
  g_arcballMat->getUniforms().put("uColor", Cvec3f(0.27f, 0.82f, 0.35f));
  g_arcballMat->getRenderStates().polygonMode(GL_FRONT_AND_BACK, GL_LINE);

  // copy solid prototype, and set to color white
  g_lightMat.reset(new Material(solid));
  g_lightMat->getUniforms().put("uColor", Cvec3f(1, 1, 1));

  // copy solid prototype, and set to color red
  g_sparkMat.reset(new Material(solid));
  g_sparkMat->getUniforms().put("uColor", Cvec3f(1, 0, 0));

  // pick shader
  g_pickingMat.reset(new Material("./shaders/basic-gl3.vshader", "./shaders/pick-gl3.fshader"));
};


static void initGeometry() {
  initGround();
  initCubes();
  initSpheres();
  initLights();
  initSparks();
  initMesh();
}

static void constructRobot(shared_ptr<SgTransformNode> base, shared_ptr<Material> material) {

  const double ARM_LEN = 0.5, 
               ARM_THICK = 0.13, 
               TORSO_LEN = 0.75, 
               TORSO_THICK = 0.35,
               TORSO_WIDTH = 0.35,
               LEG_LEN = 0.6,
               LEG_THICK = 0.13;

  const int NUM_JOINTS = 10,
            NUM_SHAPES = 10;

  struct JointDesc {
    int parent;
    float x, y, z;
  };

  JointDesc jointDesc[NUM_JOINTS] = {
    {-1}, // torso
    {0,  TORSO_WIDTH/2, TORSO_LEN*0.75, 0}, // upper right arm
    {1,  ARM_LEN, 0, 0}, // lower right arm
    {0, -1 * TORSO_WIDTH/2, TORSO_LEN*0.75, 0}, // upper left arm
    {3,  -1 * ARM_LEN, 0, 0}, // lower left arm
    {0, TORSO_WIDTH/2, -1* TORSO_LEN/2, 0},// upper right leg
    {5, LEG_LEN, 0, 0},// lower right leg
    {0, -1 * TORSO_WIDTH/2, -1*TORSO_LEN/2, 0},// upper left leg
    {7, -1 * LEG_LEN, 0, 0},  // lower left leg
    {0, 0, TORSO_LEN/8, 0} // head
  };

  struct ShapeDesc {
    int parentJointId;
    float x, y, z, sx, sy, sz;
    shared_ptr<Geometry> geometry;
  };

  ShapeDesc shapeDesc[NUM_SHAPES] = {
    {0, 0,         0, 0, TORSO_WIDTH, TORSO_LEN, TORSO_THICK, g_sphere}, // torso
    {1, ARM_LEN/2, 0, 0, ARM_LEN, ARM_THICK, ARM_THICK, g_sphere}, // upper right arm
    {2, ARM_LEN/2, 0, 0, ARM_LEN, ARM_THICK, ARM_THICK, g_sphere}, // lower right arm
    {3, -1 * ARM_LEN/2, 0, 0, ARM_LEN, ARM_THICK, ARM_THICK, g_sphere}, // upper left arm
    {4, -1 * ARM_LEN/2, 0, 0, ARM_LEN, ARM_THICK, ARM_THICK, g_sphere}, // lower left arm
    {5, LEG_LEN/2, 0, 0, LEG_LEN, LEG_THICK, LEG_THICK, g_sphere},// upper right leg
    {6, LEG_LEN/2, 0, 0, LEG_LEN, LEG_THICK, LEG_THICK, g_sphere},// lower right leg
    {7, -1 * LEG_LEN/2, 0, 0, LEG_LEN, LEG_THICK, LEG_THICK, g_sphere},// upper left leg
    {8, -1 * LEG_LEN/2, 0, 0, LEG_LEN, LEG_THICK, LEG_THICK, g_sphere}, // lower left leg
    {9, 0, 1, 0, TORSO_LEN*5/12, TORSO_LEN*5/12, TORSO_LEN*5/12, g_sphere} //head
  };

  shared_ptr<SgTransformNode> jointNodes[NUM_JOINTS];

  for (int i = 0; i < NUM_JOINTS; ++i) {
    if (jointDesc[i].parent == -1)
      jointNodes[i] = base;
    else {
      Quat q = Quat();

      if (i < 5 && jointDesc[i].parent == 0)
	q = Quat::makeZRotation(jointDesc[i].x*-200);
      else if (i < 8 && jointDesc[i].parent == 0)
	q = Quat::makeZRotation(jointDesc[i].x*-390);
      jointNodes[i].reset(new SgRbtNode(RigTForm(Cvec3(jointDesc[i].x, jointDesc[i].y, jointDesc[i].z),q)));
      jointNodes[jointDesc[i].parent]->addChild(jointNodes[i]);
    }
  }
  for (int i = 0; i < NUM_SHAPES; ++i) {
    shared_ptr<SgGeometryShapeNode> shape(
      new SgGeometryShapeNode(
			      shapeDesc[i].geometry, 
			      material, 
			      Cvec3(shapeDesc[i].x, shapeDesc[i].y, shapeDesc[i].z),
			      Cvec3(0, 0, 0),
			      Cvec3(shapeDesc[i].sx, shapeDesc[i].sy, shapeDesc[i].sz)));
    jointNodes[shapeDesc[i].parentJointId]->addChild(shape);
  }
}

static void initScene() {
  g_world.reset(new SgRootNode());

  g_skyNode.reset(new SgRbtNode(RigTForm(Cvec3(0.0, 0.0, 2.0))));

  g_groundNode.reset(new SgRbtNode());
  g_groundNode->addChild(shared_ptr<SgGeometryShapeNode>(
							 new SgGeometryShapeNode(g_ground, g_greenDiffuseMat, Cvec3(0, g_groundY, 0))));

  // defining subDiv node
  g_firework1Node.reset(new SgRbtNode(RigTForm(Cvec3(-0.5,0,0))));
  g_firework1Node->addChild(shared_ptr<SgGeometryShapeNode>(new SgGeometryShapeNode(g_firework1, g_fireworkMat, Cvec3(0, 0, 0)))); 

  g_firework2Node.reset(new SgRbtNode(RigTForm(Cvec3(0.5,0,0))));
  g_firework2Node->addChild(shared_ptr<SgGeometryShapeNode>(new SgGeometryShapeNode(g_firework2, g_fireworkMat, Cvec3(0, 0, 0)))); 
  

  /*g_robot1Node.reset(new SgRbtNode(RigTForm(Cvec3(-1.1, 0.3, 0))));
  g_robot2Node.reset(new SgRbtNode(RigTForm(Cvec3(1.1, 0.3, 0))));

  constructRobot(g_robot1Node, g_redDiffuseMat); // a Purple robot
  constructRobot(g_robot2Node, g_blueDiffuseMat); // a Blue robot*/

  g_world->addChild(g_skyNode);
  g_world->addChild(g_groundNode);
  //g_world->addChild(g_robot1Node);
  //g_world->addChild(g_robot2Node);
  g_world->addChild(g_firework1Node);
  g_world->addChild(g_firework2Node);

  // add lights
  g_light1Node.reset(new SgRbtNode(RigTForm(Cvec3(-1, 1.5, 1))));
  g_light2Node.reset(new SgRbtNode(RigTForm(Cvec3(1, 1.5, 1))));

  g_world->addChild(g_light1Node);
  g_world->addChild(g_light2Node);
  g_light1Node->addChild(shared_ptr<SgGeometryShapeNode>(new SgGeometryShapeNode(g_light, g_lightMat, Cvec3(0, 0, 0))));
  g_light2Node->addChild(shared_ptr<SgGeometryShapeNode>(new SgGeometryShapeNode(g_light, g_lightMat, Cvec3(0, 0, 0))));
  
  // g_currEye = g_skyNode;
}

int main(int argc, char * argv[]) {
  try {
    initGlutState(argc,argv);

    glewInit(); // load the OpenGL extensions

    cout << (g_Gl2Compatible ? "Will use OpenGL 2.x / GLSL 1.0" : "Will use OpenGL 3.x / GLSL 1.3") << endl;
    if ((!g_Gl2Compatible) && !GLEW_VERSION_3_0)
      throw runtime_error("Error: card/driver does not support OpenGL Shading Language v1.3");
    else if (g_Gl2Compatible && !GLEW_VERSION_2_0)
      throw runtime_error("Error: card/driver does not support OpenGL Shading Language v1.0");

    initGLState();
    initMaterials();
    initGeometry();
    initScene();
    setEye();
    g_currentPickedRbtNode = g_skyNode;
    dumpSgRbtNodes(g_world,g_nodes);

    initFirework1();
    initFirework2();

    animateFirework1(0);

    animateFirework2(0);

    //animateMeshCube(0);

    glutMainLoop();
    return 0;
  }
  catch (const runtime_error& e) {
    cout << "Exception caught: " << e.what() << endl;
    return -1;
  }
}
